﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TgifOLD2.aspx.cs" Inherits="VisualRecognitionWebApplication.Tgif1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    
    <link href="Content/jquery.bootgrid.min.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
		<table id="grid-data" class="table table-condensed table-hover table-striped" data-toggle="bootgrid" data-ajax="true" data-url="server.php">
		<thead>
            <tr>
                <th data-column-id="id" data-type="numeric" data-identifier="true">id</th>
                <th data-column-id="movie">Movie</th>
				<th data-column-id="year" data-type="numeric">year</th>
				<th data-column-id="genre">Genre</th>
				<th data-column-id="rating_imdb" data-type="numeric">Rating</th>
            </tr>
		</thead>	
</table>
	
	
	<button onclick="getServerData()">Refresh Data</button> 
	<button onclick="clearGrid()">Clear table</button> 

    

    <!-- Include bootgrid plugin (below), -->
    <script src="Scripts/jquery.bootgrid.min.js"></script>
  <!-- now write the script specific for this grid -->
  	<script lang="javascript">
	//Refer to http://jquery-bootgrid.com/Documentation for methods, events and settings

	//load gird on page\e load...
	$("#grid-data").bootgrid(
	 {
	 caseSensitive:false
	 
	 });
	
	function getServerData()
	{
	 console.log("getServerData");
	 $("#grid-data").bootgrid({ caseSensitive:false});
	}
	
	function clearGrid()
	{
	console.log("clearGrid");
	$("#grid-data").bootgrid().clear();
	}
	
	</script>
</asp:Content>
