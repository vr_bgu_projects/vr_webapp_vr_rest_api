﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualRecognitionWebApplication.Helpers;
using VisualRecognitionWebApplication.Models;

namespace VisualRecognitionWebApplication
{
    public partial class Image : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string image_id = Request.QueryString["image_id"];
                string db = Request.QueryString["db"];
                if (image_id != null && db != null)
                {
                    imagePanelTitle.InnerText = string.Format("Image ID: {0}, DB: {1} - Ground truth", image_id, db);
                    Tgif tgif = ImageUtil.GetTgifData(image_id);
                    if (tgif != null)
                    {
                        LabelImageDesc.Text = tgif.Image_Description;
                        LabelImageId.Text = tgif.Id.ToString();
                        HyperLinkImageUrl.Text = tgif.Image_Url;
                        HyperLinkImageUrl.NavigateUrl = tgif.Image_Url;
                        imgPlaceholder.Src = tgif.Image_Url;

                        PopulateMsOxfordResult(image_id);
                        PopulateOpenCalaisResult(image_id);
                        
                    }
                }
            }
        }

        private void PopulateOpenCalaisResult(string image_id)
        {
            OpenCalaisResult tgifOpenCalaisResult = ImageUtil.GetTgifOpenCalaisData(image_id);
            if (tgifOpenCalaisResult != null)
            {
                //buttonMSOxford.CssClass = "btn btn-default";
                LabelOpenCalaisResult.Text = tgifOpenCalaisResult.result;
                LabelOpenCalaisResultsDate.Text = tgifOpenCalaisResult.result_date;
            }
        }

        private void PopulateMsOxfordResult(string image_id)
        {
            TgifMsOxfordResult tgifMsOxfordResult = ImageUtil.GetTgifMsOxfordData(image_id);
            if (tgifMsOxfordResult != null)
            {
                buttonMSOxford.CssClass = "btn btn-default";
                LabelMsOxfordId.Text = tgifMsOxfordResult.Id.ToString();
                LabelMsOxfordCategories.Text = ImageUtil.GetTgifMsOxfordCategories(tgifMsOxfordResult.Analysis_Result.Categories);
                LabelMsOxfordColor.Text = Util.PrintObjectPropertiesRecursive(tgifMsOxfordResult.Analysis_Result.Color);
                LabelMsOxfordImageType.Text = Util.PrintObjectPropertiesRecursive(tgifMsOxfordResult.Analysis_Result.ImageType);
                LabelMsOxfordFaces.Text = ImageUtil.GetTgifMsOxfordFaces(tgifMsOxfordResult.Analysis_Result.Faces);
                LabelMsOxfordAdult.Text = Util.PrintObjectPropertiesRecursive(tgifMsOxfordResult.Analysis_Result.Adult);
                LabelMsOxfordImageDescription.Text = ImageUtil.GetTgifMsOxfordDescriptions(tgifMsOxfordResult.Describe_Result.description);
                LabelMsOxfordTags.Text = ImageUtil.GetTgifMsOxfordTags(tgifMsOxfordResult.Tag_Result.tags);

                LabelMsOxfordResultsDate.Text = tgifMsOxfordResult.Date_Added;
            }

        }

        protected void buttonMSOxford_Click(object sender, EventArgs e)
        {

            var client = new RestClient("http://vrbguapi.azurewebsites.net/api/MSOxford/AnalyzeImage");
            var request = new RestRequest(Method.GET);
            request.AddParameter("image_url", HyperLinkImageUrl.Text);
            request.AddParameter("visualFeatures", "Tags,categories,adult,color,imageType,faces");

            var queryResult = client.Execute(request);

            

            //TODO: API FROM CONFIG
            MSOxford.MSOxfordClient msOxfordClient = new MSOxford.MSOxfordClient("9519f36e663e4980952ac8d9a3113b2b");
            //var result = msOxfordClient.AnalyzeImageByUrl(HyperLinkImageUrl.ImageUrl).Result;
        }

        protected void buttonClarifai_Click(object sender, EventArgs e)
        {
            var client = new RestClient("https://api.thomsonreuters.com/permid/calais");

            var request = new RestRequest(Method.POST);
            //TODO: to config file
            request.AddHeader("x-ag-access-token", "qFyf7M6qICBD9U6XEor0SA748MgroaqY"); 
            request.AddHeader("outputFormat", "application/json");
            request.AddHeader("Content-Type", "text/plain");
            
            // easy async support
            client.ExecuteAsync(request, response => {
                LabelOpenCalaisResult.Text = response.Content;
            });

            
        }
    }
}