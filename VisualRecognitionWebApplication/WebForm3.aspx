﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="VisualRecognitionWebApplication.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

        <br />
        <br />
        <div class="container">
            <h1 class="text-capitalize text-center">Register</h1>
            <div class="table-responsive">
                <table class="table table-bordered table-striped">
                    <tr>
                        <th>Id:</th>
                        <td>
                            <asp:TextBox ID="id" runat="server" CssClass="form-control"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <th>Image Url:</th>
                        <td>
                            <asp:TextBox ID="image_url" runat="server" CssClass="form-control"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <th>Image Description:</th>
                        <td>
                            <asp:TextBox ID="image_description" runat="server" CssClass="form-control"></asp:TextBox></td>
                    </tr>                   
                    <%--<tr>
                        <td class="text-right" colspan="2"><asp:Button ID="btn_clear" runat="server" Text="Clear" CssClass="btn btn-primary" OnClick="btn_clear_Click" />
                        <asp:Button ID="btn_Save" runat="server" Text="Save" OnClick="btn_Save_Click" CssClass="btn btn-primary" />
                        </td>
                    </tr>--%>
                </table>
            </div>
            <asp:GridView ID="Grid" runat="server" CssClass="table table-bordered table-striped" OnRowDeleting="Grid_RowDeleting" OnRowEditing="Grid_RowEditing" OnRowCancelingEdit="Grid_RowCancelingEdit" OnRowUpdating="Grid_RowUpdating">
                <Columns>
<%--                    <asp:CommandField ButtonType="Link" HeaderText="Edit/Delete" ShowEditButton="true" ShowDeleteButton="true" ItemStyle-Width="100" />--%>
                </Columns>
            </asp:GridView>
        </div>

</asp:Content>
