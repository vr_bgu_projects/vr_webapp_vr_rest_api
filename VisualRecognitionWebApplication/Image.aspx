﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Image.aspx.cs" Inherits="VisualRecognitionWebApplication.Image" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/jquery.bootgrid.css" rel="stylesheet" />
    <link href="Content/font-awesome.css" rel="stylesheet" />
    <style>
        img {
            display: block;
            max-width: 230px;
            max-height: 150px;
            width: auto;
            height: auto;
        }
    </style>

    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <form runat="server">

        <div class="panel panel-primary" style="width: 80%; margin: 20px">
            <div class="panel-heading">
                <h3 id="imagePanelTitle" class="panel-title" runat="server">Image Details -Ground truth</h3>
            </div>
            <div class="panel-body">
                <span class="label label-info">Image Id</span>
                <asp:Label ID="LabelImageId" runat="server" Text=""></asp:Label>
                <br />

                <span class="label label-info">Image Description</span>
                <%--            <blockquote class="blockquote">--%>
                <asp:Label ID="LabelImageDesc" runat="server" Text=""></asp:Label>
                <%--            </blockquote>--%>

                <br />
                <span class="label label-info">Image Url</span>
                <asp:HyperLink ID="HyperLinkImageUrl" runat="server">#</asp:HyperLink>
                <%--            <asp:Label ID="LabelImageUrl" runat="server" Text=""></asp:Label>--%>
                <br />

                <img runat="server" id="imgPlaceholder" src="#" alt="#" class="img-thumbnail img" />
            </div>

            <div class="panel-footer">
                <asp:Button ID="buttonMSOxford" type="button" runat="server"  class="btn btn-success" Text="MS Oxford" OnClick="buttonMSOxford_Click"></asp:Button>
                <asp:Button ID="buttonClarifai" type="button" runat="server"  class="btn btn-success" Text="Clarifai" OnClick="buttonClarifai_Click"></asp:Button>
            </div>

        </div>



        <div class="panel panel-primary" style="width: 80%; margin: 20px">
            <div class="panel-heading">
                <h3 id="H1" class="panel-title" runat="server">MsOxford Results - </h3> <asp:Label ID="LabelMsOxfordResultsDate" style="white-space:nowrap;" runat="server"></asp:Label>
            </div>
            <div class="panel-body">
                <span class="label label-default">Image Id</span>
                <asp:Label ID="LabelMsOxfordId" runat="server" Text=""></asp:Label>
                <br />

                <span class="label label-default">Image Description</span>
                <asp:Label ID="LabelMsOxfordImageDescription" runat="server" Text=""></asp:Label>

                <%--<br />
                <span class="label label-default">Image Url</span>
<%--                <asp:HyperLink ID="HyperLinkMsOxfordImageUrl" runat="server"></asp:HyperLink>--%>
                
                <br />
                <span class="label label-default">Tags</span>
                <asp:Label ID="LabelMsOxfordTags" runat="server"></asp:Label>
                  <br />
                <span class="label label-default">Categories</span>
                <asp:Label ID="LabelMsOxfordCategories" runat="server"></asp:Label>

                 <br />
                <span class="label label-default">Faces</span>
                <asp:Label ID="LabelMsOxfordFaces" runat="server"></asp:Label>

                 <br />
                <span class="label label-default">Color</span>
                <asp:Label ID="LabelMsOxfordColor" runat="server"></asp:Label>

                 <br />
                <span class="label label-default">ImageType</span>
                <asp:Label ID="LabelMsOxfordImageType" runat="server"></asp:Label>

                 <br />
                <span class="label label-default">Adult</span>
                <asp:Label ID="LabelMsOxfordAdult" runat="server"></asp:Label>
            </div>

            <div class="panel-footer">
            </div>

        </div>



        <div class="panel panel-primary" style="width: 80%; margin: 20px">
            <div class="panel-heading">
                <h3 id="H2" class="panel-title" runat="server">OpenCalais Results - </h3> <asp:Label style="white-space:nowrap;" ID="LabelOpenCalaisResultsDate" runat="server"></asp:Label>
            </div>
            <div class="panel-body">
                <span class="label label-default">Image Id</span>
                <asp:Label ID="LabelOpenCalaisResult" runat="server" Text=""></asp:Label>
                <br />               
            </div>

            <div class="panel-footer">
            </div>

        </div>
    </form>


      <!-- Scripts -->
    <script src="Scripts/jquery-1.11.1.js"></script>
    <script src="Scripts/bootstrap.js"></script>
            <script src="Scripts/utils.js"></script>

    <script>



        //$.ajax({
        //    url: 'https://api.thomsonreuters.com/permid/calais',
        //    headers: {
        //        'outputFormat': 'application/json',
        //        'x-ag-access-token': 'qFyf7M6qICBD9U6XEor0SA748MgroaqY',
        //        'Content-Type': 'text/plain'
        //    },
        //    method: 'POST',
        //    data: "Wearable camera maker GoPro Inc's chief executive, Nicholas Woodman, plans to sell a portion of hisstake as part of an $800 million offering of the company's shares.",
        //    success: function (data) {
        //        console.log('succes: ' + data);
        //    }
        //});
    </script>
</asp:Content>
