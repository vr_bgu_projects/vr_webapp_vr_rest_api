﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SQLite;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using VisualRecognitionWebApplication.Helpers;

namespace VisualRecognitionWebApplication
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        SqlLiteClient sqlLiteClient;
        SQLiteCommand cmd;
        SQLiteDataAdapter DB;
        DataSet DS = new DataSet();
        DataTable DT = new DataTable();

        protected void Page_Load(object sender, EventArgs e)
        {
            sqlLiteClient = new SqlLiteClient();
            if (!Page.IsPostBack)
            {
                LoadData();
            }
        }

        //protected void btn_Save_Click(object sender, EventArgs e)
        //{
        //    sqlLiteClient.Openconn();
        //    cmd = sqlLiteClient.Conn.CreateCommand();
        //    cmd.CommandText = "insert into student (Name,EmailID,Address,PhoneNO) values('" + txt_Name.Text.Trim() + "','" + txt_EmailID.Text.Trim() + "','" + txt_Address.Text.Trim() + "','" + txt_Phone.Text.Trim() + "')";
        //    cmd.ExecuteNonQuery();
        //    txt_Name.Text = "";
        //    txt_EmailID.Text = "";
        //    txt_Address.Text = "";
        //    txt_Phone.Text = "";
        //    sqlLiteClient.Closeconn();
        //    LoadData();
        //}

        //protected void btn_clear_Click(object sender, EventArgs e)
        //{
        //    txt_Name.Text = "";
        //    txt_EmailID.Text = "";
        //    txt_Address.Text = "";
        //    txt_Phone.Text = "";
        //}

        private void LoadData()
        {
            sqlLiteClient.OpenConn();
            cmd = sqlLiteClient.Conn.CreateCommand();
            string CommandText = "SELECT * FROM tgif ORDER BY id ASC LIMIT 0, 1000";
            DB = new SQLiteDataAdapter(CommandText, sqlLiteClient.Conn);
            DS.Reset();
            DB.Fill(DS);
            DT = DS.Tables[0];
            Grid.DataSource = DT;
            Grid.DataBind();
            sqlLiteClient.CloseConn();
        }

        protected void Grid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                GridViewRow row = (GridViewRow)Grid.Rows[e.RowIndex];
                var id = row.Cells[1].Text.ToString();
                sqlLiteClient.OpenConn();
                cmd = new SQLiteCommand("delete from student where id=" + id + "", sqlLiteClient.Conn);
                cmd.ExecuteNonQuery();
                sqlLiteClient.CloseConn();
                LoadData();
            }
        }

        protected void Grid_RowEditing(object sender, GridViewEditEventArgs e)
        {
            var id = Grid.Rows[e.NewEditIndex].Cells[1].Text.ToString();
            LoadData();

        }

        protected void Grid_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = Grid.Rows[e.RowIndex];
            sqlLiteClient.OpenConn();
            cmd = new SQLiteCommand("update student set name = '" + ((TextBox)(row.Cells[2].Controls[0])).Text + "', emailid = '" + ((TextBox)(row.Cells[3].Controls[0])).Text + "',address = '" + ((TextBox)(row.Cells[4].Controls[0])).Text + "', phoneno = '" + ((TextBox)(row.Cells[5].Controls[0])).Text + "' where id = '" + ((TextBox)(row.Cells[1].Controls[0])).Text + "'", sqlLiteClient.Conn);
            cmd.ExecuteNonQuery();
            sqlLiteClient.CloseConn();
            Grid.EditIndex = -1;
            LoadData();
        }

        protected void Grid_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            Grid.EditIndex = -1;
            LoadData();
        }

    }
}