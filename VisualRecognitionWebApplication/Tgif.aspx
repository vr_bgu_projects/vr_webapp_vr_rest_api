﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tgif.aspx.cs" Inherits="VisualRecognitionWebApplication.WebForm5" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <!-- Styles -->
    <link href="Content/bootstrap.css" rel="stylesheet" />
    <link href="Content/jquery.bootgrid.css" rel="stylesheet" />
    <link href="Content/font-awesome.css" rel="stylesheet" />
    <style>
        .tgifTable{
            width:90%;
            margin:20px;
        }

    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   



        <table id="grid" class="table table-condensed table-hover table-striped tgifTable">
        <thead>
            <tr>
                <th data-column-id="Id" data-type="numeric" data-identifier="true">ID</th>
                <th data-column-id="Image_Url">Image_Url</th>
                <th data-column-id="Image_Description">Image_Description</th>
                <th data-column-id="link" data-formatter="link" data-sortable="false">Link</th>
                <th data-column-id="details" data-formatter="details" data-sortable="false">Details</th>

<%--                <th data-column-id="commands" data-formatter="commands" data-sortable="false">Commands</th>--%>
            </tr>
        </thead>
    </table>
  


    <!-- Button trigger modal -->
<%--<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  Launch demo modal
</button>--%>


        <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Modal title</h4>
          </div>
          <div class="modal-body">
            ...
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </div>
    </div>

      <!-- Scripts -->
    <script src="Scripts/jquery-1.11.1.js"></script>
    <script src="Scripts/bootstrap.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/8.4.0/jquery.mark.js"></script>
    <script src="Scripts/jquery.bootgrid.js"></script>
    <script src="Scripts/Tgif.js"></script>
            <script src="Scripts/utils.js"></script>

    <script>
        ActiveNav("tgifNav");
    </script>
</asp:Content>
