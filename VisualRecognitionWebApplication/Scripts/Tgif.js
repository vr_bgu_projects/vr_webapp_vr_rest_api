﻿
$("#grid").bootgrid({
    searchSettings: {
        delay: 250,
        characters: 3
    },
    ajax: true,
    ajaxSettings: {
        method: "POST",
        cache: true
    },
    selection: true,
    multiSelect: true,
    sorting: false,
    post: function () {
        /* To accumulate custom parameter with the request object */
        return {
            id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
        };
    },
    url: "http://vrbguapi.azurewebsites.net/api/Tgif/PostTgifData",
    formatters: {
        "link": function (column, row) {
            return '<a target ="_blank" href="'+row.Image_Url+'">Open Image</a>';
        },
        "details": function (column, row) {
            //return '<a target ="_blank" href="/Image.aspx?image_id=' + row.Image_Url + '&db=tgif">Open Image</a>';
            //return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-toggle=\"modal\" data-target=\"#myModal\" data-row-id=\"" + row.Id + "\"><span class=\"fa fa-pencil\"></span></button> "
            return "<button type='button' class='btn btn-xs btn-default' onClick=\"window.open('Image.aspx?image_id=" + row.Id + "&db=tgif');\" data-row-id='" + row.Id + "'><span class='fa fa-pencil'></span></button> "

        },
        "commands": function (column, row) {
            return "<button type=\"button\" class=\"btn btn-xs btn-default command-edit\" data-toggle=\"modal\" data-target=\"#myModal\" data-row-id=\"" + row.Id + "\"><span class=\"fa fa-pencil\"></span></button> " +
                "<button type=\"button\" class=\"btn btn-xs btn-default command-delete\" data-row-id=\"" + row.Id + "\"><span class=\"fa fa-trash-o\"></span></button>";
        }
    }
}).on("selected.rs.jquery.bootgrid", function (e, rows) {
    var rowIds = [];
    for (var i = 0; i < rows.length; i++) {
        rowIds.push(rows[i].id);
    }
    alert("Select: " + rowIds.join(","));
}).on("deselected.rs.jquery.bootgrid", function (e, rows) {
    var rowIds = [];
    for (var i = 0; i < rows.length; i++) {
        rowIds.push(rows[i].id);
    }
    alert("Deselect: " + rowIds.join(","));
});


var grid = $("#grid-command-buttons").bootgrid({
    ajax: true,
    post: function () {
        return {
            id: "b0df282a-0d67-40e5-8558-c9e93b7befed"
        };
    },
    url: "http://vrbguapi.azurewebsites.net/api/Tgif/PostTgifData"
}).on("loaded.rs.jquery.bootgrid", function () {
    /* Executes after data is loaded and rendered */
    grid.find(".command-edit").on("click", function (e) {
        alert("You pressed edit on row: " + $(this).data("row-id"));
    }).end().find(".command-delete").on("click", function (e) {
        alert("You pressed delete on row: " + $(this).data("row-id"));
    });
});

