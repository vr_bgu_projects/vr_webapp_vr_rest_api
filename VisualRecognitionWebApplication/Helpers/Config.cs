﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using System.Web;
using System.IO;

namespace VisualRecognitionWebApplication.Helpers
{
    public sealed class Config
    {
        private static Config instance = null;

        private static readonly object padlock = new object();

        Config()
        {}

        public static Config Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {

                        instance = new Config();
                    }
                    return instance;
                }
            }
        }

        private static XmlDocument configuration = null;

        public static void LoadingConfiguration()
        {
            try
            {
                string configFilePath = GetConfigFilePath();
                Constants.ConnectionString = Config.GetConfigurationValue(configFilePath, "DateBase", "ConnectionString");
                
                Constants.DBName = Config.GetConfigurationValue(configFilePath, "DateBase", "DBName");
               
                Constants.LogFilePath = Config.GetConfigurationValue(configFilePath, "General", "LogFilePath");
               
            }
            catch (Exception ex)
            {
                Logger.log.ErrorFormat("Error while trying to load constants values, see error details\n{0}", ex);
            }

        }

        public static string GetConfigFilePath()
        {
            try
            {
                string webAppPath = HttpContext.Current.Server.MapPath("~/bin");

                //string webAppPath = HttpRuntime.AppDomainAppPath + "bin";
                var configFilePath = Path.Combine(webAppPath, "REFBOT.config");
                return configFilePath;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static string GetConfigurationValue(string filePath, string sectionnName, string settingName)
        {
            try
            {
                if (configuration == null)
                {
                    configuration = new XmlDocument();
                    string fileContent = System.IO.File.ReadAllText(filePath);
                    configuration.PreserveWhitespace = false;
                    configuration.LoadXml(fileContent);
                    //Logger.Log("Configuration file content: " + configuration.InnerXml);
                }
                XmlNode node = configuration.DocumentElement.SelectSingleNode("/configuration/" + sectionnName + "/" + settingName);
                return node.InnerText;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return "";
        }

        public static XmlNodeList GetConfigurationList(string filePath, string sectionnName, string settingName)
        {
            try
            {
                if (configuration == null)
                {
                    configuration = new XmlDocument();
                    string fileContent = System.IO.File.ReadAllText(filePath);
                    configuration.PreserveWhitespace = false;
                    configuration.LoadXml(fileContent);
                    Logger.log.Debug("Configuration file content: " + configuration.InnerXml);
                }
                XmlNodeList nodes = configuration.DocumentElement.SelectNodes("/configuration/" + sectionnName + "/" + settingName);
                return nodes;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return null;
        }
    }
}
