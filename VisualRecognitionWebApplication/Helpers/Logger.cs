﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using VisualRecognitionWebApplication.Helpers;
using System.Threading.Tasks;

namespace VisualRecognitionWebApplication.Helpers
{
    public static class Logger
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        public static bool CleanLog()
        {
            bool result = false;
            try
            {
                string logFilePath = Constants.LogFilePath;
                if (File.Exists(logFilePath))
                {
                    File.WriteAllText(Constants.LogFilePath, "");
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex.ToString());
                File.WriteAllText(Constants.LogFilePath, "");

            }

            return result;
        }

        public static void LogIncomingHttpContentMessageAsDebug(dynamic data, string openingMessage)
        {
            if (data != null && !string.IsNullOrEmpty(data.ToString()))
            {
                string message = String.Format("{0}\n{1}", openingMessage, data.ToString());
                Task.Run(() => LogMessageToDebug(message));
            }
        }

        public static void LogMessageToDebug(string message)
        {
            log.Debug(message);
        }
    }
}
