﻿using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using VisualRecognitionWebApplication.Models;

namespace VisualRecognitionWebApplication.Helpers
{
    public class ImageUtil
    {
        public static Tgif GetTgifData(string id)
        {
            //curl - X GET--header 'Accept: application/json' 'http://vrbguapi.azurewebsites.net/api/Tgif/GetTgifImageData?image_id=10'
            var client = new RestClient("http://vrbguapi.azurewebsites.net/api/Tgif/GetTgifImageData");
            // client.Authenticator = new HttpBasicAuthenticator(username, password);

            var request = new RestRequest(Method.GET);
            request.AddParameter("image_id", id); // adds to POST or URL querystring based on Method

            // easily add HTTP Headers
            request.AddHeader("Accept", "application/json");

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            var tgif = JsonConvert.DeserializeObject<Tgif>(content);

            // or automatically deserialize result
            // return content type is sniffed but can be explicitly set via RestClient.AddHandler();
            //IRestResponse<Tgif> response2 = client.Execute<Tgif>(request);

            //// easy async support
            //client.ExecuteAsync(request, response => {
            //    Console.WriteLine(response.Content);
            //});

            //// async with deserialization
            //var asyncHandle = client.ExecuteAsync<Tgif>(request, response => {
            //});

            //// abort the request on demand
            //asyncHandle.Abort();

            return tgif;
        }

        public static string GetTgifMsOxfordCategories(List<Category> categories)
        {
            StringBuilder result = new StringBuilder();
            if (categories !=null && categories.Count> 0)
            {
                foreach(Category cat in categories)
                {
                    string catResult = string.Format("{0} <{1}>, ",cat.Name, cat.Score);
                    result.Append(catResult);

                }
            }
            return result.ToString();
        }



        public static string GetTgifMsOxfordFaces(List<Face> faces)
        {
            StringBuilder result = new StringBuilder();
            if (faces != null && faces.Count > 0)
            {
                foreach (Face face in faces)
                {
                    string catResult = Util.PrintObjectPropertiesRecursive(face);
                    result.Append(catResult);

                }
            }
            return result.ToString();
        }

        public static string GetTgifMsOxfordDescriptions(Description description)
        {
            StringBuilder result = new StringBuilder();
            if (description != null && description.captions.Count > 0)
            {
                string delimeter = "<BR />";
                result.Append(delimeter);
                int numbering = 1;
                
                foreach (Caption caption in description.captions)
                {
                    if (numbering == description.captions.Count)
                        delimeter = "";
                    string captionResult = string.Format("{0}. {1} <{2}>{3}",numbering, caption.text, caption.confidence, delimeter);
                    result.Append(captionResult);
                    numbering++;
                }
            }
            return result.ToString();
        }

       

        public static string GetTgifMsOxfordTags(List<Tag> tags)
        {
            StringBuilder result = new StringBuilder();
            if (tags != null && tags.Count > 0)
            {
                string delimeter = ", ";
                result.Append(delimeter);
                int numbering = 1;

                foreach (Tag tag in tags)
                {
                    if (numbering == tags.Count)
                        delimeter = "";
                    string tagResult = string.Format("{0}. {1} <{2}>{3}", numbering, tag.name, tag.confidence, delimeter);
                    result.Append(tagResult);
                    numbering++;
                }
            }
            return result.ToString();
        }

        public static TgifMsOxfordResult GetTgifMsOxfordData(string id)
        {
            var client = new RestClient("http://vrbguapi.azurewebsites.net/api/MSOxford/GetTgifIMsOxfordData");

            var request = new RestRequest(Method.GET);
            request.AddParameter("image_id", id); // adds to POST or URL querystring based on Method

            // easily add HTTP Headers
            request.AddHeader("Accept", "application/json");

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            var tgifMsOxfordResult = JsonConvert.DeserializeObject<TgifMsOxfordResult>(content);

            

            return tgifMsOxfordResult;
        }


        public static OpenCalaisResult GetTgifOpenCalaisData(string id)
        {
            var client = new RestClient("http://vrbguapi.azurewebsites.net/api/OpenCalais/GetTgifIOpenCalaisData");

            var request = new RestRequest(Method.GET);
            request.AddParameter("image_id", id); // adds to POST or URL querystring based on Method

            // easily add HTTP Headers
            request.AddHeader("Accept", "application/json");

            // execute the request
            IRestResponse response = client.Execute(request);
            var content = response.Content; // raw content as string
            var tgifOpenCalaisResult = JsonConvert.DeserializeObject<OpenCalaisResult>(content);



            return tgifOpenCalaisResult;
        }
    }
}