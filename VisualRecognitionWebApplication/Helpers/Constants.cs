﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualRecognitionWebApplication.Helpers
{
    public class Constants
    {
        public static string ConnectionString { get; set; }
        public static string DBName { get; set; }
        public static string LogFilePath { get; set; }
    }
}