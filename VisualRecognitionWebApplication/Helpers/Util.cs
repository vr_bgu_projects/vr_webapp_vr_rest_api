﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;

namespace VisualRecognitionWebApplication.Helpers
{
    public static class Util
    {
        public static string Html2Text(string html)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(@"<html><body>" + html + "</body></html>");
            return doc.DocumentNode.SelectSingleNode("//body").InnerText.Replace(Environment.NewLine, " ");
        }

        public static string GetWebApplicationPath()
        {
            return System.IO.Path.GetDirectoryName(HttpRuntime.AppDomainAppPath);
        }

        public static string GetVirtualPath(string physicalPath)
        {
            if (!physicalPath.StartsWith(HttpContext.Current.Request.PhysicalApplicationPath))
            {
                throw new InvalidOperationException("Physical path is not within the application root");
            }

            return "~/" + physicalPath.Substring(HttpContext.Current.Request.PhysicalApplicationPath.Length)
                  .Replace("\\", "/");
        }

        public static string CleanText(string text)
        {
            string cleanText = text.Replace('\t', ' ').Trim();
            if (String.IsNullOrEmpty(cleanText))
                return "";
            if (!cleanText.Any(c => char.IsLetterOrDigit(c))) //does not contain the numbers & characters 0-9 and A-Z? 
            {
                return "";
            }
            return cleanText;
        }

        public static string GetLastXlinesFromFile(string filePath, int numOfLines = 10)
        {
            string result = "";
            try
            {
                if (IsFileExists(filePath))
                {
                    int neededLines = numOfLines;
                    var lines = new List<String>();
                    foreach (var s in File.ReadLines(filePath))
                    {
                        lines.Add(s);
                        if (lines.Count > neededLines)
                        {
                            lines.RemoveAt(0);
                        }
                    }

                    result = String.Join(Environment.NewLine, lines);
                }

            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static bool ContainsIgnoreCase(string substring, string str)
        {
            if (str.IndexOf(substring, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return true;
            }
            return false;
        }

        public static bool ContainsIgnoreCase(string substring, StringBuilder sb)
        {
            if (sb.ToString().IndexOf(substring, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return true;
            }
            return false;
        }

        public static int FindIgnoreCase(StringBuilder sb, string substring)
        {
            int pos = sb.ToString().IndexOf(substring, StringComparison.OrdinalIgnoreCase);
            if (pos >= 0)
            {
                return pos;
            }
            return pos;
        }

        public static int FindIgnoreCase(string str, string substring)
        {
            int pos = str.IndexOf(substring, StringComparison.OrdinalIgnoreCase);
            if (pos >= 0)
            {
                return pos;
            }
            return pos;
        }

        private static string AppendAtPosition(string baseString, int position, string character)
        {
            var sb = new StringBuilder(baseString);
            for (int i = position; i < sb.Length; i += (position + character.Length))
                sb.Insert(i, character);
            return sb.ToString();
        }

        public static string ReplaceNonAlphaNumericCharacters(string str, string replaceWith)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            str = rgx.Replace(str, replaceWith);
            return str;
        }

        public static string RemoveInvalidFilePathCharacters(string filename, string replaceChar)
        {
            Regex r;
            try
            {
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                return filename;
            }
            return r.Replace(filename, replaceChar);
        }

        public static bool IsFileLocked(string filePath)
        {
            FileInfo file = new FileInfo(filePath);
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (FileNotFoundException)
            {
                return false;
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static bool IsFileExists(string fullPath)
        {
            try
            {
                if (File.Exists(fullPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsDirectoryExists(string fullPath)
        {
            try
            {
                if (Directory.Exists(fullPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsNumeric(string num)
        {
            int n;
            bool isNumeric = int.TryParse(num, out n);
            return isNumeric;
        }
        public static List<string> TakeLastLines(string text, int count)
        {
            List<string> lines = new List<string>();
            Match match = Regex.Match(text, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);

            while (match.Success && lines.Count < count)
            {
                lines.Insert(0, match.Value);
                match = match.NextMatch();
            }

            return lines;
        }

        public static MemoryStream GenerateStreamFromString(string value)
        {
            return new MemoryStream(Encoding.UTF8.GetBytes(value ?? ""));
        }

        public static string TakeLastLine(string text)
        {
            List<string> lines = new List<string>();
            Match match = Regex.Match(text, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);

            while (match.Success && lines.Count < 1)
            {
                lines.Insert(0, match.Value);
                match = match.NextMatch();
            }

            return lines[0];
        }

        public static List<string> ReadAllFilesNamesInfolder(string folderPath, string ext)
        {
            List<string> filesPathList = new List<string>();
            ext = ext.Replace(".", "").ToLower(); //If we got the ext with the dot
            try
            {
                DirectoryInfo dir = new DirectoryInfo(folderPath);
                foreach (FileInfo flInfo in dir.GetFiles())
                {
                    if (flInfo.Extension.ToLower() == ext || flInfo.Extension.ToLower() == "." + ext)
                    {
                        String name = flInfo.Name;
                        filesPathList.Add(name);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }

            return filesPathList;
        }

        public static string GetLineFromFile(string fileName, int line)
        {
            using (var sr = new StreamReader(fileName))
            {
                for (int i = 1; i < line; i++)
                    sr.ReadLine();
                return sr.ReadLine();
            }
        }

        public static string GetLineFromString(string text, int lineNo)
        {
            string[] lines = text.Replace("\r", "").Split('\n');
            return lines.Length >= lineNo ? lines[lineNo - 1] : null;
        }

        public static string GetLinesRangeFromString(string text, int fromLineNo, int toLineNo)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                for (int i = fromLineNo; i <= toLineNo; i++)
                {
                    sb.AppendLine(GetLineFromString(text, i));
                }
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return sb.ToString();
        }

        public static string ReadFileContent(string filePath)
        {
            string readText = null;
            try
            {
                if (File.Exists(filePath))
                {
                    readText = System.IO.File.ReadAllText(filePath);
                    return readText;
                }

            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }

            return readText;
        }

        public static string ConvertDataTableToHTML(DataTable dt)
        {
            string html = "<table>";
            //add header row
            html += "<tr>";
            for (int i = 0; i < dt.Columns.Count; i++)
                html += "<td>" + dt.Columns[i].ColumnName + "</td>";
            html += "</tr>";
            //add rows
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                html += "<tr>";
                for (int j = 0; j < dt.Columns.Count; j++)
                    html += "<td>" + dt.Rows[i][j].ToString() + "</td>";
                html += "</tr>";
            }
            html += "</table>";
            return html;
        }

        public static string GetCellValue(DataRow row, string columnName)
        {
            if (row[columnName] != null)
                return ReplaceEmptyStringWithNA(row[columnName].ToString());
            return "N/A";
        }

        public static string ReplaceNewLineOrTabWithSpace(string input, string replacementString)
        {
            string result = Regex.Replace(input, @"\r\n?|\n|\t", replacementString);
            return result;
        }

        public static string ReplaceAllLineBreaksToUnixLineBreak(string input)
        {
            string result = Regex.Replace(input, @"\r\n?|\n", "\n");
            return result;
        }
        public static string ReplaceEmptyStringWithNA(string value)
        {
            string cleanValue = value.Trim();
            cleanValue = String.IsNullOrEmpty(value) ? "N/A" : value;
            return cleanValue;
        }

        public static bool IsFileEmpty(string filePath)
        {
            bool result = false;
            if (File.Exists(filePath) && new FileInfo(filePath).Length == 0)
            {
                result = true;
            }
            return result;
        }

        public static bool IsOdd(int value)
        {
            return value % 2 != 0;
        }

        public static bool IsEven(int value)
        {
            return value % 2 == 0;
        }

        //Function to get random number
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }

        public static string ConvertDataTableToString(DataTable dataTable, bool isCentered = true)
        {
            var output = new StringBuilder();

            var columnsWidths = new int[dataTable.Columns.Count];

            // Get column widths
            foreach (DataRow row in dataTable.Rows)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var length = row[i].ToString().Length;
                    if (columnsWidths[i] < length)
                        columnsWidths[i] = length;
                }
            }

            // Get Column Titles
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var length = dataTable.Columns[i].ColumnName.Length;
                if (columnsWidths[i] < length)
                    columnsWidths[i] = length;
            }

            // Write Column titles
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var text = dataTable.Columns[i].ColumnName;
                if (isCentered)
                    output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                else
                    output.Append(" | " + text);
            }
            output.Append("|\n" + new string('=', output.Length) + "\n");

            // Write Rows
            foreach (DataRow row in dataTable.Rows)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var text = row[i].ToString();
                    if (isCentered)
                        output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                    else
                        output.Append(" | " + text);
                }

                output.Append("|\n");
            }
            return output.ToString();
        }

        private static string PadCenter(string text, int maxLength)
        {
            int diff = maxLength - text.Length;
            return new string(' ', diff / 2) + text + new string(' ', (int)(diff / 2.0 + 0.5));

        }

        public static string ObjectToJsonString(object obj)
        {
            string output = JsonConvert.SerializeObject(obj);
            return output;

        }

        /// <summary>
        /// Retrieve the description on the enum, e.g.
        /// [Description("Bright Pink")]
        /// BrightPink = 2,
        /// Then when you pass in the enum, it will retrieve the description
        /// </summary>
        /// <param name="en">The Enumeration</param>
        /// <returns>A string representing the friendly name</returns>
        public static string GeEnumDescription(Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }

        public static bool IsValidUrl(string urlString)
        {
            Uri uri;
            return Uri.TryCreate(urlString, UriKind.Absolute, out uri)
                && (uri.Scheme == Uri.UriSchemeHttp
                 || uri.Scheme == Uri.UriSchemeHttps
                 || uri.Scheme == Uri.UriSchemeFtp
                 || uri.Scheme == Uri.UriSchemeMailto
                 || Uri.IsWellFormedUriString(urlString, UriKind.RelativeOrAbsolute));
        }

        public static object StringToJsonObject(string str)
        {
            JObject json = JObject.Parse(str);
            return json;

        }

        public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            return httpContext;
        }

        public static string GetObjectPropertiesAndValues(object obj, bool isNewLine = false)
        {
            StringBuilder sb = new StringBuilder();

            if (obj != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    string name = descriptor.Name;
                    object value = descriptor.GetValue(obj);
                    if (isNewLine)
                        sb.AppendLine(string.Format("{0}={1}", name, value));
                    else
                        sb.Append(string.Format("{0}={1}, ", name, value));
                }
            }

            return sb.ToString();
        }


        public static string PrintObjectPropertiesRecursive(object obj)
        {
            string str = "";
            StringBuilder sb = new StringBuilder();

            if (obj != null)
            {
                str = PrintObjectPropertiesRecursive(obj, 0, sb);
            }

            return str;
        }
        public static string PrintObjectPropertiesRecursive(object obj, int indent, StringBuilder sb)
        {

            if (obj == null) return "";
            string indentString = new string(' ', indent);
            Type objType = obj.GetType();
            PropertyInfo[] properties = objType.GetProperties();
            foreach (PropertyInfo property in properties)
            {
                object propValue = property.GetValue(obj, null);
                if (property.PropertyType.Assembly == objType.Assembly && !property.PropertyType.IsEnum)
                {
                    sb.AppendLine(string.Format("{0}{1}:", indentString, property.Name));
                    PrintObjectPropertiesRecursive(propValue, indent + 2, sb);
                }
                else
                {
                    if (CheckIsObjectArrayOfStrings(propValue))
                        propValue = string.Join(", ", propValue);
                    sb.AppendLine(string.Format("{0}{1}: {2}, ", indentString, property.Name, propValue));
                }
            }

            return sb.ToString();


        }

        private static bool CheckIsObjectArrayOfStrings(object propValue)
        {
            var expectedType = typeof(string);

            Type valueType = propValue.GetType();
            if (valueType.IsArray && expectedType.IsAssignableFrom(valueType.GetElementType()))
            {
                return true;
            }

            return false;
        }


        /// <summary>
        /// Sending GET request.
        /// </summary>
        /// <param name="Url">Request Url.</param>
        /// <param name="queryString">Data for request.</param>
        /// <returns>Response body.</returns>
        public static string HTTP_GET(string Url, string queryString, string contentType, Dictionary<string, string> headersDic = null)
        {
            string Out = String.Empty;
            System.Net.WebRequest req = System.Net.WebRequest.Create(Url + (string.IsNullOrEmpty(queryString) ? "" : "?" + queryString));
            try
            {
                req.ContentType = contentType;
                if (headersDic != null && headersDic.Count > 0)
                {
                    foreach (KeyValuePair<string, string> entry in headersDic)
                    {
                        req.Headers.Add(entry.Value, entry.Key);
                    }
                }
                System.Net.WebResponse resp = req.GetResponse();
                using (System.IO.Stream stream = resp.GetResponseStream())
                {
                    using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
                    {
                        Out = sr.ReadToEnd();
                        sr.Close();
                    }
                }
            }
            catch (ArgumentException ex)
            {
                Out = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
            }
            catch (WebException ex)
            {
                Out = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Out = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
            }

            return Out;
        }

        /// <summary>
        /// Sending POST request.
        /// </summary>
        /// <param name="Url">Request Url.</param>
        /// <param name="Data">Data for request.</param>
        /// <returns>Response body.</returns>
        public static string HTTP_POST_PUT(string Url, string Data, string contentType, string method, Dictionary<string,string> headersDic = null)
        {
            string Out = String.Empty;
            System.Net.WebRequest req = System.Net.WebRequest.Create(Url);
            try
            {
                req.Method = method;
                req.Timeout = 100000;
                req.ContentType = contentType;
                if (headersDic != null && headersDic.Count > 0)
                {
                    foreach (KeyValuePair<string, string> entry in headersDic)
                    {
                        req.Headers.Add(entry.Value, entry.Key);
                    }
                }
                byte[] sentData = Encoding.UTF8.GetBytes(Data);
                req.ContentLength = sentData.Length;
                using (System.IO.Stream sendStream = req.GetRequestStream())
                {
                    sendStream.Write(sentData, 0, sentData.Length);
                    sendStream.Close();
                }
                System.Net.WebResponse res = req.GetResponse();
                System.IO.Stream ReceiveStream = res.GetResponseStream();
                using (System.IO.StreamReader sr = new System.IO.StreamReader(ReceiveStream, Encoding.UTF8))
                {
                    Char[] read = new Char[256];
                    int count = sr.Read(read, 0, 256);

                    while (count > 0)
                    {
                        String str = new String(read, 0, count);
                        Out += str;
                        count = sr.Read(read, 0, 256);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                Out = string.Format("HTTP_ERROR :: The second HttpWebRequest object has raised an Argument Exception as 'Connection' Property is set to 'Close' :: {0}", ex.Message);
            }
            catch (WebException ex)
            {
                Out = string.Format("HTTP_ERROR :: WebException raised! :: {0}", ex.Message);
            }
            catch (Exception ex)
            {
                Out = string.Format("HTTP_ERROR :: Exception raised! :: {0}", ex.Message);
            }

            return Out;
        }


    }
}