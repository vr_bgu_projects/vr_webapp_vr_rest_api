﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="VisualRecognitionWebApplication.WebForm2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

    <%--<script src="Scripts/jquery-2.1.1.min.js"></script>
    <script src="Scripts/jqGrid/grid.locale-en.js"></script>
    <script src="Scripts/jqGrid/jquery.jqGrid.min.js"></script>

    <link href="Content/ui.jqgrid.css" rel="stylesheet" />

    <script>
        
   
		$.jgrid.defaults.width = 780;
		$.jgrid.defaults.styleUI = 'Bootstrap';
	</script>--%>



     <link type="text/css" href="http://jqueryrock.googlecode.com/svn/trunk/css/jquery-ui-1.9.2.custom.css" rel="stylesheet" />  
    <link type="text/css" href="http://jqueryrock.googlecode.com/svn/trunk/jqgrid/css/ui.jqgrid.css" rel="stylesheet" />  
    <script type="text/javascript" src="http://jqueryrock.googlecode.com/svn/trunk/js/jquery-1.8.3.js"></script>  
    <script type="text/javascript" src="http://jqueryrock.googlecode.com/svn/trunk/js/jquery-ui-1.9.2.custom.js"></script>  
    <script src="http://jqueryrock.googlecode.com/svn/trunk/jqgrid/js/grid.locale-en.js" type="text/javascript"></script>  
    <script src="http://jqueryrock.googlecode.com/svn/trunk/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>  
  
  
    <script type="text/javascript">  
  
        $(function () {  
            $("#dataGrid").jqGrid({  
                url: 'Default.aspx/GetDataFromDB',  
                datatype: 'json',  
                mtype: 'POST',  
  
                serializeGridData: function (postData) {  
                    return JSON.stringify(postData);  
                },  
  
                ajaxGridOptions: { contentType: "application/json" },  
                loadonce: true,  
                colNames: ['Employee ID', 'Name', 'Designation', 'City', 'State', 'Country'],  
                colModel: [  
                                { name: 'Emp_ID', index: 'Employee ID', width: 80 },  
                                { name: 'Name', index: 'Name', width: 140 },  
                                { name: 'Designation', index: 'Designation', width: 160 },  
                                { name: 'City', index: 'City', width: 180 },  
                                { name: 'State', index: 'State', width: 180 },  
                                { name: 'Country', index: 'Country', width: 180 }  
                ],  
                pager: '#pagingGrid',  
                rowNum: 5,  
                rowList: [10, 20, 30],  
                viewrecords: true,  
                gridview: true,  
                jsonReader: {  
                    page: function (obj) { return 1; },  
                    total: function (obj) { return 1; },  
                    records: function (obj) { return obj.d.length; },  
                    root: function (obj) { return obj.d; },  
                    repeatitems: false,  
                    id: "0"  
                },  
                caption: 'jQ Grid Example'  
            });  
        }).pagingGrid("#pager", { edit: true, add: true, del: false });  
  
  
    </script>  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<%--    <div style="margin-left:20px">
    <table id="jqGrid"></table>
    <div id="jqGridPager"></div>
</div>
    <script type="text/javascript"> 
    
        $(document).ready(function () {
            $.noConflict(true); // <-- true removes the reference to jQuery aswell.

            jQuery.browser = {};
            (function () {
                jQuery.browser.msie = false;
                jQuery.browser.version = 0;
                if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
                    jQuery.browser.msie = true;
                    jQuery.browser.version = RegExp.$1;
                }
            })();


            $("#jqGrid").jqGrid({
                colModel: [
                    {
						label: 'Title',
                        name: 'Title',
                        width: 150,
                        formatter: formatTitle
                    },
                    {
						label: 'Link',
                        name: 'Link',
                        width: 80,
                        formatter: formatLink
                    },
                    {
						label: 'View Count',
                        name: 'ViewCount',
                        width: 35,
						sorttype:'integer',
						formatter: 'number',
						align: 'right'
                    },
                    {
						label: 'Answer Count',
                        name: 'AnswerCount',
                        width: 25
                    }
                ],

                viewrecords: true, // show the current page, data rang and total records on the toolbar
                width: 780,
                height: 200,
                rowNum: 15,
				datatype: 'local',
                pager: "#jqGridPager",
				caption: "Load live data from stackoverflow"
            });

            fetchGridData();

            function fetchGridData() {
                
                var gridArrayData = [];
				// show loading message
				$("#jqGrid")[0].grid.beginReq();
                $.ajax({
                    url: "http://api.stackexchange.com/2.2/questions?order=desc&sort=activity&tagged=jqgrid&site=stackoverflow",
                    success: function (result) {
                        for (var i = 0; i < result.items.length; i++) {
                            var item = result.items[i];
                            gridArrayData.push({
                                Title: item.title,
                                Link: item.link,
                                CreationDate: item.creation_date,
                                ViewCount: item.view_count,
                                AnswerCount: item.answer_count
                            });                            
                        }
						// set the new data
						$("#jqGrid").jqGrid('setGridParam', { data: gridArrayData});
						// hide the show message
						$("#jqGrid")[0].grid.endReq();
						// refresh the grid
						$("#jqGrid").trigger('reloadGrid');
                    }
                });
            }

            function formatTitle(cellValue, options, rowObject) {
                return cellValue.substring(0, 50) + "...";
            };

            function formatLink(cellValue, options, rowObject) {
                return "<a href='" + cellValue + "'>" + cellValue.substring(0, 25) + "..." + "</a>";
            };

            

        });

    </script>--%>

    <table style="border: solid 15px red; width: 100%; vertical-align: central;">  
        <tr>  
            <td style="padding-left: 20px; padding-top: 20px; padding-bottom: 20px; background-color: skyblue; font-family: 'Times New Roman'; font-weight: bold; font-size: 20pt; color: chocolate;">jQ Grid Example In  ASP.NET C#  
            </td>  
        </tr>  
        <tr>  
            <td style="text-align: center; vertical-align: central; padding: 50px;">  
                <table id="dataGrid" style="text-align: center;"></table>  
                <div id="pagingGrid"></div>  
            </td>  
        </tr>  
    </table>  

</asp:Content>
