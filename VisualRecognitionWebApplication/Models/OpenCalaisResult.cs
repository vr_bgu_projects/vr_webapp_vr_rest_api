﻿namespace VisualRecognitionWebApplication.Models
{
    public class OpenCalaisResult
    {
        public string image_id { get; set; }
        public string image_desc { get; set; }
        public string result { get; set; }
        public string result_date { get; set; }
    }
}