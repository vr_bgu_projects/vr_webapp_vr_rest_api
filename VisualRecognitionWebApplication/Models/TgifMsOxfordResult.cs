﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualRecognitionWebApplication.Models
{
    public class TgifMsOxfordResult
    {
        public int Id { get; set; }
        public string Image_Url { get; set; }
        public string Date_Added { get; set; }
        public AnalysisResult Analysis_Result { get; set; }
        public DescribeResult Describe_Result { get; set; }
        public TagResult Tag_Result { get; set; }
    }


    public class Metadata
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string Format { get; set; }
    }

    public class ImageType
    {
        public int ClipArtType { get; set; }
        public int LineDrawingType { get; set; }
    }

    public class Color
    {
        public string AccentColor { get; set; }
        public string DominantColorForeground { get; set; }
        public string DominantColorBackground { get; set; }
        public List<string> DominantColors { get; set; }
        public bool IsBWImg { get; set; }
    }

    public class Adult
    {
        public bool IsAdultContent { get; set; }
        public bool IsRacyContent { get; set; }
        public double AdultScore { get; set; }
        public double RacyScore { get; set; }
    }

    public class Category
    {
        public string Name { get; set; }
        public double Score { get; set; }
    }

    public class FaceRectangle
    {
        public int Width { get; set; }
        public int Height { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
    }

    public class Face
    {
        public int Age { get; set; }
        public string Gender { get; set; }
        public FaceRectangle FaceRectangle { get; set; }
    }

    public class AnalysisResult
    {
        public string RequestId { get; set; }
        public Metadata Metadata { get; set; }
        public ImageType ImageType { get; set; }
        public Color Color { get; set; }
        public Adult Adult { get; set; }
        public List<Category> Categories { get; set; }
        public List<Face> Faces { get; set; }
    }

    public class Caption
    {
        public string text { get; set; }
        public double confidence { get; set; }
    }

    public class Description
    {
        public List<string> tags { get; set; }
        public List<Caption> captions { get; set; }
    }

    public class Metadata2
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string Format { get; set; }
    }

    public class DescribeResult
    {
        public Description description { get; set; }
        public string requestId { get; set; }
        public Metadata2 metadata { get; set; }
    }

    public class Tag
    {
        public string name { get; set; }
        public double confidence { get; set; }
    }

    public class Metadata3
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string Format { get; set; }
    }

    public class TagResult
    {
        public List<Tag> tags { get; set; }
        public string requestId { get; set; }
        public Metadata3 metadata { get; set; }
    }

 

}