﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualRecognitionWebApplication.Models
{
    public class Row:Tgif
    {
        //public int id { get; set; }
        //public string sender { get; set; }
        //public string received { get; set; }
    }

    public class TgifTable
    {
        public int current { get; set; }
        public int rowCount { get; set; }
        public List<Row> rows { get; set; }
        public int total { get; set; }
    }
}