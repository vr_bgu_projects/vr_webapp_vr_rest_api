﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="VisualRecognitionWebApplication.WebForm1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
         <script src="Scripts/jquery-1.11.1.min.js"></script>
         <script src="Scripts/bootstrap.min.js"></script>
    <link href="Content/mdb.css" rel="stylesheet" />
    <link href="Content/style.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <div class="container">

      <div class="starter-template">
        <h1>Visual Recognition BGU</h1>
        <p class="lead">Use this document as a way to quickly start any new project.<br> Currently, we are using <a href="https://github.com/raingo/TGIF-Release" target="_blank">Tgif dataset</a>, </p>
      </div>

        

    <script src="Scripts/utils.js"></script>
    <script src="Scripts/mdb.min.js"></script>
    <script>
        ActiveNav("homeNav");
    </script>
</asp:Content>
