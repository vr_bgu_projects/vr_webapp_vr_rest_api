﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VisualRecognitionAPI.Helpers;

namespace VisualRecognitionAPI.Controllers
{
    public class DataBaseController : ApiController
    {
        public string GetDataBaseInfo()
        {
            string result = "";
            try
            {
                SqlLiteClient sqlClient = new SqlLiteClient();
                DataTable dtResults = sqlClient.ReturnDataTable("SELECT * FROM sqlite_master WHERE type='table'");

                result = Util.ConvertDataTableToString(dtResults);
            }
            catch (Exception ex)
            {

                result = ex.ToString();
            }
            return result;
        }
    }
}
