﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using VisualRecognitionAPI.Helpers;

namespace VisualRecognitionAPI.Controllers
{
    public class OpenCalaisController : ApiController
    {
        [HttpGet]
        [ActionName("GetTgifIOpenCalaisData")]
        public HttpResponseMessage GetTgifIOpenCalaisData(string image_id)
        {
            StringBuilder sb = new StringBuilder();
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                TgifOpenCalaisResult tgifOpenCalaisResult = TgifUtil.GetTgifOpenCalaisData(image_id);
                Logger.log.DebugFormat("Received the GetTgifIOpenCalaisData command. Image Id" + image_id);
                result.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(tgifOpenCalaisResult), Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                result.Content = new StringContent(ex.ToString());
                Logger.log.Error(ex);
            }
            finally
            {
            }
            return result;

        }
    }
}
