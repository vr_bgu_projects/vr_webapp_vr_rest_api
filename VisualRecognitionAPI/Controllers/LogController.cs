﻿using VisualRecognitionAPI.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace VisualRecognitionAPI.Controllers
{
    public class LogController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetLog()
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                var path = System.Web.HttpContext.Current.Server.MapPath(Util.GetVirtualPath(Constants.LogFilePath)); 
                var stream = new FileStream(path, FileMode.Open);

                result.Content = new StreamContent(stream);
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                result.Content.Headers.ContentDisposition.FileName = Path.GetFileName(path);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentLength = stream.Length;  
                if (stream.Length == 0)
                    result.Content = new StringContent("The log file is empty.");
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return result;
        }

        [HttpGet]
        public HttpResponseMessage CleanLog()
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            string resultText = "";
            try
            {
                Logger.CleanLog();
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                resultText = ex.InnerException.ToString();
                result.Content = new StringContent(resultText);
                return result;
            }
            resultText = "Log cleaned.";
            result.Content = new StringContent(resultText);
            return result;
        }

    }
}
