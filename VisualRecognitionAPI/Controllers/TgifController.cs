﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Web.Http;
using VisualRecognitionAPI.Helpers;
using VisualRecognitionAPI.Models;
using VisualRecognitionAPI.Models.TGIF;

namespace VisualRecognitionAPI.Controllers
{
    public class TgifController : ApiController
    {
        [HttpGet]
        [ActionName("GetTgifData")]
        public HttpResponseMessage GetTgifData()
        {
            StringBuilder sb = new StringBuilder();
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                TgifTable tgifTable = TgifUtil.GetTgifTable();
                Logger.log.DebugFormat("Received the GetTgifData command.");
                result.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(tgifTable), Encoding.UTF8, "application/json");

            }
            catch (Exception ex)
            {
                result.Content = new StringContent(ex.ToString());
                Logger.log.Error(ex);
            }
            finally
            {
            }
            return result;

        }

        [HttpGet]
        [ActionName("GetTgifImageData")]
        public HttpResponseMessage GetTgifImageData(string image_id)
        {
            StringBuilder sb = new StringBuilder();
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                Tgif tgif = TgifUtil.GetTgifImageData(image_id);
                Logger.log.DebugFormat("Received the GetTgifImageData command. Image Id" + image_id);
                result.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(tgif), Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                result.Content = new StringContent(ex.ToString());
                Logger.log.Error(ex);
            }
            finally
            {
            }
            return result;

        }

        [HttpPost]
        [ActionName("PostTgifData")]
        public HttpResponseMessage PostTgifData(FormDataCollection formData)
        {
            StringBuilder sb = new StringBuilder();
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            try
            {
                NameValueCollection valueMap = Util.ConvertFormDataCollection(formData);
                //var current = valueMap["current"];
                //var rowCount = valueMap["rowCount.Value"];
                //var searchPhrase = valueMap["searchPhrase"];
                //var id = valueMap["id"];

                TgifTable tgifTable = TgifUtil.GetTgifTable(valueMap);
                Logger.log.DebugFormat("Received the GetTgifData command.");
                result.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(tgifTable), Encoding.UTF8, "application/json");

            }
            catch (Exception ex)
            {
                result.Content = new StringContent(ex.ToString());
                Logger.log.Error(ex);
            }
            finally
            {
            }
            return result;

        }
    }
}