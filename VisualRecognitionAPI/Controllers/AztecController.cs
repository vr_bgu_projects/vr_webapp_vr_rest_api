﻿using VisualRecognitionAPI.Helpers;
using VisualRecognitionAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;

namespace VisualRecognitionAPI.Controllers
{
    public class AztecController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage GetListItemById([FromBody]dynamic data)
        {
            string list_name = data.list_name;
            string item_id = data.id;
            HttpResponseMessage resultResponse = new HttpResponseMessage(HttpStatusCode.OK);
            string result = "";
            try
            {
                if (String.IsNullOrEmpty(list_name) || String.IsNullOrEmpty(item_id))
                {
                    Logger.log.Info("Empty list name or item id");
                    resultResponse.Content = new StringContent(String.Format("Bla", Constants.NotFoundResultsMessage));
                    return resultResponse;
                }
                else
                    list_name = Uri.UnescapeDataString(list_name); //escaping e.g., replacing %20 = space

                DataTable dt = null;


                result = "";
                resultResponse = Request.CreateResponse(HttpStatusCode.OK, result);
                return resultResponse;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                resultResponse.Content = new StringContent("Error occurred, see log.");
                return resultResponse;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetListItemsByName(string list_name, string item_name)
        {
            HttpResponseMessage resultResponse = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                if (String.IsNullOrEmpty(list_name) || String.IsNullOrEmpty(item_name))
                {
                    Logger.log.Info("Empty list name or item name");
                    resultResponse.Content = new StringContent(String.Format("Bla", Constants.NotFoundResultsMessage));
                    return resultResponse;
                }
                else
                    list_name = Uri.UnescapeDataString(list_name); //escaping e.g., replacing %20 = space

                DataTable dt = null;
                resultResponse = Request.CreateResponse(HttpStatusCode.OK, dt);
                return resultResponse;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                resultResponse.Content = new StringContent("Error occurred, see log.");
                return resultResponse;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetListItemsByQuery(string list_name, string query)
        {
            HttpResponseMessage resultResponse = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                if (String.IsNullOrEmpty(list_name) || String.IsNullOrEmpty(query))
                {
                    Logger.log.Info("Empty list name or query");
                    resultResponse.Content = new StringContent(String.Format("Bla", Constants.NotFoundResultsMessage));
                    return resultResponse;
                }
                else
                    list_name = Uri.UnescapeDataString(list_name); //escaping e.g., replacing %20 = space

                DataTable dt = null;
                resultResponse = Request.CreateResponse(HttpStatusCode.OK, dt);
                return resultResponse;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                resultResponse.Content = new StringContent("Error occurred, see log.");
                return resultResponse;
            }
        }

    }
}
