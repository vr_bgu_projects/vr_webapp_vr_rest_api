﻿using VisualRecognitionAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace VisualRecognitionAPI.Controllers
{
    public class HelpController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage Help()
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            string resultText = "";
            try
            {
                resultText = Constants.HelpText.Replace("\r\n", "\n");
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                resultText = ex.InnerException.ToString();
                result.Content = new StringContent(resultText);
                return result;
            }
            result.Content = new StringContent(resultText);
            return result;
        }


        [HttpPost]
        public HttpResponseMessage Post([FromBody]dynamic data)
        {
            HttpResponseMessage resultResponse = new HttpResponseMessage(HttpStatusCode.OK);
            string resultText = "";
            try
            {
                string value = data.command;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                resultResponse.Content = new StringContent("Error occurred, see log.");
                return resultResponse;
            }

            resultText = "No Command";
            resultResponse.Content = new StringContent(resultText);
            return resultResponse;
        }


    }
}
