﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using VisualRecognitionAPI.Helpers;

namespace VisualRecognitionAPI.Controllers
{
    public class ConfigController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetConfigFile()
        {
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            string resultText = "";
            try
            {
                resultText = Config.GetConfigFileContent();
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                resultText = ex.InnerException.ToString();
                result.Content = new StringContent(resultText);
                return result;
            }
            result.Content = new StringContent(resultText);
            return result;
        }
    }
}
