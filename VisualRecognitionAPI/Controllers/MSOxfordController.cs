﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Web.Http;
using VisualRecognitionAPI.Helpers;
using VisualRecognitionAPI.Models;
using VisualRecognitionAPI.Models.TGIF;
using Microsoft.ProjectOxford.Vision;
using System.Threading.Tasks;

namespace VisualRecognitionAPI.Controllers
{
    public class MSOxfordController : ApiController
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="image_url"></param>
        /// <param name="visualFeatures">A string indicating what visual feature types to return. Multiple values should be comma-separated.
        /// Valid visual feature types include: Categories - categorizes image content according to a taxonomy defined in documentation. Tags - tags the image with a detailed list of words related to the image content. Description - describes the image content with a complete English sentence. Faces - detects if faces are present. If present, generate coordinates, gender and age. ImageType - detects if image is clipart or a line drawing. Color - determines the accent color, dominant color, and whether an image is black&white. Adult - detects if the image is pornographic in nature (depicts nudity or a sex act). Sexually suggestive content is also detected.</param>
        /// <returns></returns>
        [HttpGet]
        [ActionName("AnalyzeImage")]
        public async Task<HttpResponseMessage> AnalyzeImage(string image_url,string visualFeatures = "Categories")
        {
            StringBuilder sb = new StringBuilder();
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                image_url = "https://s-media-cache-ak0.pinimg.com/564x/9e/66/91/9e669152378ba2e23298696acc79c9a1.jpg";
                //TODO: API IN CONFIG FILE
                MSOxfordClient msOxfordClient = new MSOxfordClient("9519f36e663e4980952ac8d9a3113b2b");
                //var resultAnalysis = msOxfordClient.AnalyzeImageByUrl(image_url);
                //var resultAnalysis = msOxfordClient.AnalyzeImageAsync(image_url,visualFeatures.Split(',')).Result;

                //MSOxfordClient msOXfordClient = new MSOxfordClient("9519f36e663e4980952ac8d9a3113b2b");
                //VisualFeature[] vfList = new VisualFeature[] { VisualFeature.Categories};
                //var sheker = await msOXfordClient.AnalyzeUrl(image_url);
                var resultAnalysis = msOxfordClient.AnalyzeImageCurl(image_url,visualFeatures);
                Logger.log.DebugFormat("Received the AnalyzeImage MS Oxford command. Image Id" + image_url);
                //result.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(resultAnalysis), Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                result.Content = new StringContent(ex.ToString());
                Logger.log.Error(ex);
            }
            finally
            {
            }
            return result;
        }

        [HttpGet]
        [ActionName("GetTgifIMsOxfordData")]
        public HttpResponseMessage GetTgifIMsOxfordData(string image_id)
        {
            StringBuilder sb = new StringBuilder();
            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);

            try
            {
                TgifMsOxfordResult tgifMsOxfordResult = TgifUtil.GetTgifMsOxfordData(image_id);
                Logger.log.DebugFormat("Received the GetTgifIMsOxfordData command. Image Id" + image_id);
                result.Content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(tgifMsOxfordResult), Encoding.UTF8, "application/json");
            }
            catch (Exception ex)
            {
                result.Content = new StringContent(ex.ToString());
                Logger.log.Error(ex);
            }
            finally
            {
            }
            return result;

        }

    }
}