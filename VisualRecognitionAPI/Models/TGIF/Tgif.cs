﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualRecognitionAPI.Models.TGIF
{
    public class Tgif
    {
        public int Id { get; set; }
        public string Image_Url { get; set; }
        public string Image_Description { get; set; }
    }
}