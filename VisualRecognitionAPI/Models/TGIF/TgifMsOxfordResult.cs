﻿///using Microsoft.ProjectOxford.Vision.Contract;
using VisualRecognitionAPI.Models.MSOXFORD;

namespace VisualRecognitionAPI.Helpers
{
    public class TgifMsOxfordResult
    {
        public int Id { get; set; }
        public string Image_Url { get; set; }
        public string Date_Added  { get; set; }
        public AnalysisResult Analysis_Result { get; set; }
        public DescribeResult Describe_Result { get; set; }

        public TagResult Tag_Result { get; set; }

    }
}