﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualRecognitionAPI.Models.TGIF
{
    public class Row:Tgif
    {
   
    }

    public class TgifTable
    {
        public int current { get; set; }
        public int rowCount { get; set; }
        public List<Row> rows { get; set; }
        public int total { get; set; }
    }
}