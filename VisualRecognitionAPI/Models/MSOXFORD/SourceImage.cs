﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VisualRecognitionAPI.Models.MSOXFORD
{
    public class SourceImage
    {
        public int ImageId { get; set; }
        public string ImageUrl { get; set; }
        public string ImageDesc { get; set; }

        public SourceImage()
        {              
        }
    }
}
