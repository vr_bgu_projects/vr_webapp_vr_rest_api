﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualRecognitionAPI.Models.MSOXFORD
{
   

    public class Caption
    {
        public string text { get; set; }
        public double confidence { get; set; }
    }

    public class Description
    {
        public List<string> tags { get; set; }
        public List<Caption> captions { get; set; }
    }

    [JsonObject("metadata")]
    public class MetadataDescribeResult
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string Format { get; set; }
    }

    public class DescribeResult
    {
        public Description description { get; set; }
        public string requestId { get; set; }
        public Metadata metadata { get; set; }
    }
}