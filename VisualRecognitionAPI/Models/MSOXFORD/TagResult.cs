﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualRecognitionAPI.Models.MSOXFORD
{
   


    public class Tag
    {
        public string name { get; set; }
        public double confidence { get; set; }
    }
    [JsonObject("metadata")]
    public class MetadataTagResult
    {
        public int Height { get; set; }
        public int Width { get; set; }
        public string Format { get; set; }
    }

    public class TagResult
    {
        public List<Tag> tags { get; set; }
        public string requestId { get; set; }
        public Metadata metadata { get; set; }
    }

}