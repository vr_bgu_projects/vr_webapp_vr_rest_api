﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;
using VisualRecognitionAPI.Helpers;
using System.Web;
using System.IO;

namespace VisualRecognitionAPI.Helpers
{
    public sealed class Config
    {
        private static Config instance = null;

        private static readonly object padlock = new object();

        Config()
        {}

        public static Config Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {

                        instance = new Config();
                    }
                    return instance;
                }
            }
        }

        private static XmlDocument configuration = null;        

        public static string GetConfigFilePath()
        {
            try
            {
                string webAppPath = HttpContext.Current.Server.MapPath("~/bin");

                //string webAppPath = HttpRuntime.AppDomainAppPath + "bin";
                var configFilePath = Path.Combine(webAppPath, "VisualRecognitionAPI.config");
                return configFilePath;
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static string GetConfigFileContent()
        {
            string fileContent = "";
            try
            {
                fileContent =File.ReadAllText(GetConfigFilePath());
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return fileContent;
        }

        public static string GetConfigurationValue(string filePath, string sectionnName, string settingName)
        {
            try
            {
                if (configuration == null)
                {
                    configuration = new XmlDocument();
                    string fileContent = System.IO.File.ReadAllText(filePath);
                    configuration.PreserveWhitespace = false;
                    configuration.LoadXml(fileContent);
                    //Logger.Log("Configuration file content: " + configuration.InnerXml);
                }
                XmlNode node = configuration.DocumentElement.SelectSingleNode("/configuration/" + sectionnName + "/" + settingName);
                return node.InnerText;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return "";
        }


        public static XmlNodeList GetConfigurationList(string filePath, string sectionnName, string settingName)
        {
            try
            {
                if (configuration == null)
                {
                    configuration = new XmlDocument();
                    string fileContent = System.IO.File.ReadAllText(filePath);
                    configuration.PreserveWhitespace = false;
                    configuration.LoadXml(fileContent);
                    Logger.log.Info("Configuration file content: " + configuration.InnerXml);
                }
                XmlNodeList nodes = configuration.DocumentElement.SelectNodes("/configuration/" + sectionnName + "/" + settingName);
                return nodes;
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return null;
        }

        public static void LoadingConfiguration()
        {
            string webAppPath = HttpRuntime.AppDomainAppPath + "bin";
            var configFilePath = Path.Combine(webAppPath, "aztec.config");
            Constants.LogFilePath = Config.GetConfigurationValue(configFilePath, "General", "LogFilePath");
           
        }
    }
}
