﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;

namespace VisualRecognitionAPI.Helpers
{
    public static class Util
    {

        public static string GetWebApplicationPath()
        {
            return System.IO.Path.GetDirectoryName(HttpRuntime.AppDomainAppPath);
        }

        public static string Html2Text(string html)
        {
            HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
            doc.LoadHtml(@"<html><body>" + html + "</body></html>");
            return doc.DocumentNode.SelectSingleNode("//body").InnerText.Replace(Environment.NewLine, " ");
        }

        public static string GetApplicationPath()
        {
            return System.IO.Path.GetDirectoryName(HttpRuntime.AppDomainAppPath);
        }

        public static string GetVirtualPath(string physicalPath)
        {
            if (!physicalPath.StartsWith(HttpContext.Current.Request.PhysicalApplicationPath))
            {
                throw new InvalidOperationException("Physical path is not within the application root");
            }

            return "~/" + physicalPath.Substring(HttpContext.Current.Request.PhysicalApplicationPath.Length)
                  .Replace("\\", "/");
        }

        public static string CleanText(string text)
        {
            string cleanText = text.Replace('\t', ' ').Trim();
            if (String.IsNullOrEmpty(cleanText))
                return "";
            if (!cleanText.Any(c => char.IsLetterOrDigit(c))) //does not contain the numbers & characters 0-9 and A-Z? 
            {
                return "";
            }
            return cleanText;
        }

        public static bool ContainsIgnoreCase(string substring, string str)
        {
            if (str.IndexOf(substring, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return true;
            }
            return false;
        }

        public static bool ContainsIgnoreCase(string substring, StringBuilder sb)
        {
            if (sb.ToString().IndexOf(substring, StringComparison.OrdinalIgnoreCase) >= 0)
            {
                return true;
            }
            return false;
        }

        public static int FindIgnoreCase(StringBuilder sb, string substring)
        {
            int pos = sb.ToString().IndexOf(substring, StringComparison.OrdinalIgnoreCase);
            if (pos >= 0)
            {
                return pos;
            }
            return pos;
        }

        public static int FindIgnoreCase(string str, string substring)
        {
            int pos = str.IndexOf(substring, StringComparison.OrdinalIgnoreCase);
            if (pos >= 0)
            {
                return pos;
            }
            return pos;
        }

        private static string AppendAtPosition(string baseString, int position, string character)
        {
            var sb = new StringBuilder(baseString);
            for (int i = position; i < sb.Length; i += (position + character.Length))
                sb.Insert(i, character);
            return sb.ToString();
        }

        public static string ReplaceNonAlphaNumericCharacters(string str, string replaceWith)
        {
            Regex rgx = new Regex("[^a-zA-Z0-9]");
            str = rgx.Replace(str, replaceWith);
            return str;
        }

        public static string RemoveInvalidFilePathCharacters(string filename, string replaceChar)
        {
            Regex r;
            try
            {
                string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
                r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
                return filename;
            }
            return r.Replace(filename, replaceChar);
        }

        public static bool IsFileLocked(string filePath)
        {
            FileInfo file = new FileInfo(filePath);
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (FileNotFoundException)
            {
                return false;
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static bool IsFileExists(string fullPath)
        {
            try
            {
                if (File.Exists(fullPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsDirectoryExists(string fullPath)
        {
            try
            {
                if (Directory.Exists(fullPath))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool IsNumeric(string num)
        {
            int n;
            bool isNumeric = int.TryParse(num, out n);
            return isNumeric;
        }
        public static List<string> TakeLastLines(string text, int count)
        {
            List<string> lines = new List<string>();
            Match match = Regex.Match(text, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);

            while (match.Success && lines.Count < count)
            {
                lines.Insert(0, match.Value);
                match = match.NextMatch();
            }

            return lines;
        }

        public static string TakeLastLine(string text)
        {
            List<string> lines = new List<string>();
            Match match = Regex.Match(text, "^.*$", RegexOptions.Multiline | RegexOptions.RightToLeft);

            while (match.Success && lines.Count < 1)
            {
                lines.Insert(0, match.Value);
                match = match.NextMatch();
            }

            return lines[0];
        }

        public static List<string> ReadAllFilesNamesInfolder(string folderPath, string ext)
        {
            List<string> filesPathList = new List<string>();
            ext = ext.Replace(".", "").ToLower(); //If we got the ext with the dot
            try
            {
                DirectoryInfo dir = new DirectoryInfo(folderPath);
                foreach (FileInfo flInfo in dir.GetFiles())
                {
                    if (flInfo.Extension.ToLower() == ext || flInfo.Extension.ToLower() == "." + ext)
                    {
                        String name = flInfo.Name;
                        filesPathList.Add(name);
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }

            return filesPathList;
        }

        public static string GetLineFromFile(string fileName, int line)
        {
            using (var sr = new StreamReader(fileName))
            {
                for (int i = 1; i < line; i++)
                    sr.ReadLine();
                return sr.ReadLine();
            }
        }

        public static string GetLineFromString(string text, int lineNo)
        {
            string[] lines = text.Replace("\r", "").Split('\n');
            return lines.Length >= lineNo ? lines[lineNo - 1] : null;
        }

        public static string GetLinesRangeFromString(string text, int fromLineNo, int toLineNo)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                for (int i = fromLineNo; i <= toLineNo; i++)
                {
                    sb.AppendLine(GetLineFromString(text, i));
                }
            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }
            return sb.ToString();
        }

        public static string ReadFileContent(string filePath)
        {
            string readText = null;
            try
            {
                if (File.Exists(filePath))
                {
                    readText = System.IO.File.ReadAllText(filePath);
                    return readText;
                }

            }
            catch (Exception ex)
            {
                Logger.log.Error(ex);
            }

            return readText;
        }



        public static string GetObjectPropertiesAndValues(object obj, bool isNewLine = false)
        {
            StringBuilder sb = new StringBuilder();

            if (obj != null)
            {
                foreach (PropertyDescriptor descriptor in TypeDescriptor.GetProperties(obj))
                {
                    string name = descriptor.Name;
                    object value = descriptor.GetValue(obj);
                    if (isNewLine)
                        sb.AppendLine(string.Format("{0}={1}", name, value));
                    else
                        sb.Append(string.Format("{0}={1}, ", name, value));
                }
            }

            return sb.ToString();
        }
        //Function to get random number
        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            { // synchronize
                return random.Next(min, max);
            }
        }

        /// <summary>
        /// Retrieve the description on the enum, e.g.
        /// [Description("Bright Pink")]
        /// BrightPink = 2,
        /// Then when you pass in the enum, it will retrieve the description
        /// </summary>
        /// <param name="en">The Enumeration</param>
        /// <returns>A string representing the friendly name</returns>
        public static string GeEnumDescription(Enum en)
        {
            Type type = en.GetType();

            MemberInfo[] memInfo = type.GetMember(en.ToString());

            if (memInfo != null && memInfo.Length > 0)
            {
                object[] attrs = memInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);

                if (attrs != null && attrs.Length > 0)
                {
                    return ((DescriptionAttribute)attrs[0]).Description;
                }
            }

            return en.ToString();
        }

        public static string GetCellValue(DataRow row, string columnName)
        {
            if (row[columnName] != null)
                return ReplaceEmptyStringWithNA(row[columnName].ToString());
            return "N/A";
        }

        public static string ReplaceNewLineOrTabWithSpace(string input, string replacementString)
        {
            string result = Regex.Replace(input, @"\r\n?|\n|\t", replacementString);
            return result;
        }

        public static string ReplaceAllLineBreaksToUnixLineBreak(string input)
        {
            string result = Regex.Replace(input, @"\r\n?|\n", "\n");
            return result;
        }

        public static string ReplaceEmptyStringWithNA(string value)
        {
            string cleanValue = value.Trim();
            cleanValue = String.IsNullOrEmpty(value) ? "N/A" : value;
            return cleanValue;
        }

        /// <summary>
        /// Copy the values contained in the given FormDataCollection into 
        /// a NameValueCollection instance.
        /// </summary>
        /// <param name="formDataCollection">The FormDataCollection instance. (required, but can be empty)</param>
        /// <returns>The NameValueCollection. Never returned null, but may be empty.</returns>
        public static NameValueCollection ConvertFormDataCollection(FormDataCollection formDataCollection)
        {
            //Validate.IsNotNull("formDataCollection", formDataCollection);

            IEnumerator<KeyValuePair<string, string>> pairs = formDataCollection.GetEnumerator();

            NameValueCollection collection = new NameValueCollection();

            while (pairs.MoveNext())
            {
                KeyValuePair<string, string> pair = pairs.Current;

                collection.Add(pair.Key, pair.Value);
            }

            return collection;

        }

        /// <summary>
        /// Assembles URL to call based on parameters, method and resource
        /// </summary>
        /// <param name="client">RestClient performing the execution</param>
        /// <param name="request">RestRequest to execute</param>
        /// <returns>Assembled System.Uri</returns>
        /// <remarks>
        /// RestClient's BuildUri purposefully leaves off the parameters from the uri it builds when the request 
        /// is a POST, PUT, or PATCH.  This extension method aims to undo this feature for debugging reasons
        /// </remarks>
        public static string GetFullRestUrl(this IRestClient client, IRestRequest request)
        {


            var uri = client.BuildUri(request);
            if (request.Method != Method.POST &&
                request.Method != Method.PUT &&
                request.Method != Method.PATCH)
            {
                return uri.AbsoluteUri;
            }
            else
            {
                var queryParameters = from p in request.Parameters
                                      where p.Type == ParameterType.GetOrPost
                                      select string.Format("{0}={1}", Uri.EscapeDataString(p.Name), Uri.EscapeDataString(p.Value.ToString()));
                if (!queryParameters.Any())
                {
                    return uri.AbsoluteUri;
                }
                else
                {
                    var path = uri.OriginalString.TrimEnd('/');
                    var query = string.Join("&", queryParameters);
                    return new Uri(path + "?" + query).AbsoluteUri;
                }
            }
        }



        public static string ConvertDataTableToString(DataTable dataTable, bool isCentered = true)
        {
            var output = new StringBuilder();

            var columnsWidths = new int[dataTable.Columns.Count];

            // Get column widths
            foreach (DataRow row in dataTable.Rows)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var length = row[i].ToString().Length;
                    if (columnsWidths[i] < length)
                        columnsWidths[i] = length;
                }
            }

            // Get Column Titles
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var length = dataTable.Columns[i].ColumnName.Length;
                if (columnsWidths[i] < length)
                    columnsWidths[i] = length;
            }

            // Write Column titles
            for (int i = 0; i < dataTable.Columns.Count; i++)
            {
                var text = dataTable.Columns[i].ColumnName;
                if (isCentered)
                    output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                else
                    output.Append(" | " + text);
            }
            output.Append("|\n" + new string('=', output.Length) + "\n");

            // Write Rows
            foreach (DataRow row in dataTable.Rows)
            {
                for (int i = 0; i < dataTable.Columns.Count; i++)
                {
                    var text = row[i].ToString();
                    if (isCentered)
                        output.Append("|" + PadCenter(text, columnsWidths[i] + 2));
                    else
                        output.Append(" | " + text);
                }

                output.Append("|\n");
            }
            return output.ToString();
        }

        private static string PadCenter(string text, int maxLength)
        {
            int diff = maxLength - text.Length;
            return new string(' ', diff / 2) + text + new string(' ', (int)(diff / 2.0 + 0.5));

        }

        public static string ObjectToJsonString(object obj)
        {
            string output = JsonConvert.SerializeObject(obj);
            return output;

        }

        public static string RunProcess(string processFileName, string arguments)
        {
            StringBuilder result = new StringBuilder();

            try
            {
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = processFileName,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                proc.Start();
                stopwatch.Stop();

                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine();
                    result.AppendLine(line);
                    Console.WriteLine(line);
                }
                Console.WriteLine(string.Format("Elapsed Time (ms): {0}", stopwatch.ElapsedMilliseconds));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result.ToString();
        }

    }
}