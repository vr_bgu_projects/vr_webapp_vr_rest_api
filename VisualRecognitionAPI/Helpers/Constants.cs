﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace VisualRecognitionAPI.Helpers
{
    public sealed class Constants
    {
        private static Constants instance = null;

        private static readonly object padlock = new object();

        Constants()
        { }

        public static Constants Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {

                        instance = new Constants();
                    }
                    return instance;
                }
            }
        }
        public static string connectionString { get; set; }
        public static string destTableName { get; set; }
        public static string destDBName { get; set; }
        public static string MailScriptPath { get; set; }
        public static string SlackApiToken { get; set; }
        public static string WebhookUrl { get; set; }
        //Site,Lists,Columns Configuration 
        public static string SP_Site { get; set; }



        public static string SP_UserName { get; set; }
        public static string SP_Password { get; set; }
        public static bool IsNewAuthSP360 { get; set; }

        public static string PdfReferencesTemplatePath { get; set; }
        public static bool SendEmailResults { get; set; }
        public static bool ShowPdfReferenceResults { get; set; }
        public static string PdfResultsNetpath { get; set; }

        public static string SlackTeamUrl { get; set; }
        public static string SlackBotName { get; set; }
        public static string SlackBotIconUrl { get; set; }
        public static string ProductNotFoundMessage { get; set; }
        public static string NotFoundResultsMessage { get; set; }
        public static string HelpText { get; set; }
        public static bool ShowPdfAccountDataResults { get; set; }
        public static string PdfAccountDataTemplatePath { get; internal set; }
        public static string SP_ListNameForIntegrations { get; internal set; }
        public static string SP_ListNameForSolutions { get; internal set; }
        public static string SP_QueryIntegrationsById { get; internal set; }
        public static string SP_QuerySolutionsById { get; internal set; }
        public static string SP_QuerySolutionsByName { get; internal set; }
        public static string SP_QuerySuitesByName { get; internal set; }
        public static string SP_ListNameForSuites { get; internal set; }
        public static string SP_QuerySuitesById { get; internal set; }
        public static string LogFilePath { get; internal set; }
        public static object AztecPortalUrl { get; internal set; }
        public static string ConfidentialStatement { get; internal set; }
    }
}