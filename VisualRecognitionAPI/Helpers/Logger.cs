﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using VisualRecognitionAPI.Helpers;

namespace VisualRecognitionAPI.Helpers
{
    public static class Logger
    {
        public static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        
        public static void CleanLog()
        {
            File.WriteAllText(Constants.LogFilePath, "");
        }
    }
}
