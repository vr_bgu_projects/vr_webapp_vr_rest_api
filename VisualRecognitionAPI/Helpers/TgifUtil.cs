﻿//using Microsoft.ProjectOxford.Vision.Contract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using VisualRecognitionAPI.Models;
using VisualRecognitionAPI.Models.MSOXFORD;
using VisualRecognitionAPI.Models.TGIF;

namespace VisualRecognitionAPI.Helpers
{
    public class TgifUtil
    {
        public static List<Row> GetTgifRows(DataTable dt)
        {
            //total = dt.Rows.Count,

            var convertedList = (from rw in dt.AsEnumerable()
                                 select new Row()
                                 {
                                     Id = Convert.ToInt32(rw["id"]),
                                     Image_Url = Convert.ToString(rw["image_url"]),
                                     Image_Description = Convert.ToString(rw["image_description"])
                                 }).ToList();

            return convertedList;
        }

        public static TgifOpenCalaisResult GetTgifOpenCalaisData(string id)
        {
            TgifOpenCalaisResult tgifOpenCalaisResult = new TgifOpenCalaisResult();
            try
            {
                SqlLiteClient sqlLiteClient = new SqlLiteClient();
                string query = "";
                if (!string.IsNullOrEmpty(id))
                    query = string.Format(@"SELECT id , text , result, date_added FROM tgif_opencalais_results WHERE id='{0}'", id);
                //Todo: paramterize sarchphrase


                DataTable tgifOpenCalaisResults = sqlLiteClient.Select(query);
                List<TgifOpenCalaisResult> imageResults = GetTgifOpenCalaisRows(tgifOpenCalaisResults);
                if (imageResults.Count > 0)
                    tgifOpenCalaisResult = imageResults[0];
            }
            catch (Exception)
            {
                throw;
            }
            return tgifOpenCalaisResult;
        }

        private static List<TgifOpenCalaisResult> GetTgifOpenCalaisRows(DataTable tgifOpenCalaisResults)
        {
            var convertedList = (from rw in tgifOpenCalaisResults.AsEnumerable()
                                 select new TgifOpenCalaisResult()
                                 {
                                     image_id = Convert.ToString(rw["id"]),
                                     image_desc = Convert.ToString(rw["text"]),
                                     result_date = Convert.ToString(rw["date_added"]),
                                     result = Convert.ToString(rw["result"].ToString())
                                 }).ToList();
            return convertedList;

        }

        public static List<TgifMsOxfordResult> GetTgifMsOxfordRows(DataTable dt)
        {
            var convertedList = (from rw in dt.AsEnumerable()
                                 select new TgifMsOxfordResult()
                                 {
                                     Id = Convert.ToInt32(rw["id"]),
                                     Image_Url = Convert.ToString(rw["url"]),
                                     Date_Added = Convert.ToString(rw["date_added"]),
                                     Analysis_Result = JsonConvert.DeserializeObject<AnalysisResult>((rw["analyze_result"].ToString())),
                                     Describe_Result = JsonConvert.DeserializeObject<DescribeResult>((rw["describe_result"].ToString())),
                                     Tag_Result = JsonConvert.DeserializeObject<TagResult>((rw["tag_result"].ToString()))

                                 }).ToList();

            return convertedList;
        }

        public static TgifTable GetTgifTable()
        {
            SqlLiteClient sqlLiteClient = new SqlLiteClient();
            DataTable tgifResults = ReturnTgifTable();

            TgifTable tgifTable = new TgifTable();
            tgifTable.rows = GetTgifRows(tgifResults);
            tgifTable.total = tgifResults.Rows.Count;
            tgifTable.current = 1;
            tgifTable.rowCount = 10;
            return tgifTable;
        }

        public static DataTable ReturnTgifTable()
        {
            DataTable dt = null;
            try
            {
                SqlLiteClient sqlLiteClient = new SqlLiteClient();
                string query = "SELECT * FROM tgif LIMIT 0, 1000";
                dt = sqlLiteClient.ReturnDataTable(query);
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }

        public static Tgif GetTgifImageData(string id)
        {
            Tgif tgif = tgif = new Tgif();
            try
            {
                SqlLiteClient sqlLiteClient = new SqlLiteClient();
                string query = "";
                if (!string.IsNullOrEmpty(id))
                    query = string.Format(@"SELECT id , image_url , image_description FROM tgif WHERE id='{0}'", id);
                //Todo: paramterize sarchphrase


                DataTable tgifResults = sqlLiteClient.Select(query);
                List<Row> imageResults = GetTgifRows(tgifResults);
                if (imageResults.Count > 0)
                    tgif = imageResults[0];
            }
            catch (Exception)
            {
                throw;
            }
            return tgif;
        }


        public static TgifMsOxfordResult GetTgifMsOxfordData(string id)
        {
            TgifMsOxfordResult tgifMsOxfordResult =  new TgifMsOxfordResult();
            try
            {
                SqlLiteClient sqlLiteClient = new SqlLiteClient();
                string query = "";
                if (!string.IsNullOrEmpty(id))
                    query = string.Format(@"SELECT id , url , analyze_result, describe_result, tag_result , date_added FROM tgif_oxford_results WHERE id='{0}'", id);
                //Todo: paramterize sarchphrase


                DataTable tgifMsOxfordResults = sqlLiteClient.Select(query);
                List<TgifMsOxfordResult> imageResults = GetTgifMsOxfordRows(tgifMsOxfordResults);
                if (imageResults.Count > 0)
                    tgifMsOxfordResult = imageResults[0];
            }
            catch (Exception ex)
            {
                throw;
            }
            return tgifMsOxfordResult;
        }

        public static TgifTable GetTgifTable(NameValueCollection valueMap)
        {
            SqlLiteClient sqlLiteClient = new SqlLiteClient();
            string current = GetTableParamValue(valueMap, "current");
            string rowCount = GetTableParamValue(valueMap, "rowCount");
            string searchPhrase = GetTableParamValue(valueMap, "searchPhrase");

            int numOfRowsToRetuen = Convert.ToInt32(rowCount) ;
            int currentRowToReturn = Convert.ToInt32(GetTableParamValue(valueMap, "current"));
            string query = "";
            if (string.IsNullOrEmpty(searchPhrase))
                query = string.Format(@"SELECT id , image_url , image_description FROM tgif 
                LIMIT {0}, {1}", (currentRowToReturn - 1) * 10, 100);
            else
                query = string.Format(@"SELECT id , image_url , image_description FROM tgif WHERE image_description LIKE '%{2}%'
                LIMIT {0}, {1}", (currentRowToReturn - 1) * 10, 100,searchPhrase);
            //Todo: paramterize sarchphrase

            //command.Parameters.AddWithValue("@searchPhrase", "%"+ searchPhrase + "%");
            //

            Dictionary<string, object> tableParameters = GetTableParameters(valueMap);
            DataTable tgifResults = sqlLiteClient.Select(query, tableParameters);

            TgifTable tgifTable = new TgifTable();
            tgifTable.rows = GetTgifRows(tgifResults.AsEnumerable().Take(numOfRowsToRetuen).CopyToDataTable());
            tgifTable.total = tgifResults.Rows.Count;
            tgifTable.current = currentRowToReturn;
            tgifTable.rowCount = numOfRowsToRetuen;
            return tgifTable;
        }

        public static Dictionary<string, object> GetTableParameters(NameValueCollection valueMap)
        {
            Dictionary<string, object> paramsDic = new Dictionary<string, object>();
            string[] keys = valueMap.AllKeys;
            foreach(string key in keys)
            {
                paramsDic.Add(key, valueMap[key]);
            }
            //var current = valueMap["current"];
            //var rowCount = valueMap["rowCount.Value"];
            //var searchPhrase = valueMap["searchPhrase"];
            //var id = valueMap["id"];

            return paramsDic;
        }

        public static string GetTableParamValue(NameValueCollection valueMap, string paramName)
        {
            string value = "";
            if (valueMap != null && !String.IsNullOrEmpty(paramName) && valueMap[paramName] != null)
                value= valueMap[paramName].ToString();
            return value;
        }
    }
}