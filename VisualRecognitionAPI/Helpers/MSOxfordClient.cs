﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web;
using System.Threading.Tasks;
using VisualRecognitionAPI.Models.MSOXFORD;
using VisualRecognitionAPI.Helpers;
using RestSharp;
using System.Net;
using System.IO;
using System.Dynamic;
using Newtonsoft.Json.Serialization;
using Microsoft.ProjectOxford.Vision;

namespace VisualRecognitionAPI.Helpers
{
    public class MSOxfordClient
    {
        /// <summary>
        /// The subscription key
        /// </summary>

        string APIKey { get; set; }
        public enum VisualFeatures { ImageType, Faces, Adult, Categories, Color, Tags, Description };


        public async Task<vrbugwinapp.Contract.AnalysisResult> AnalyzeUrl(string imageUrl)
        {
            // -----------------------------------------------------------------------
            // KEY SAMPLE CODE STARTS HERE
            // -----------------------------------------------------------------------

            //
            // Create Project Oxford Vision API Service client
            //
            Microsoft.ProjectOxford.Vision.VisionServiceClient VisionServiceClient = new Microsoft.ProjectOxford.Vision.VisionServiceClient(APIKey);
            Logger.log.Debug("VisionServiceClient is created");

            
            VisualFeature[] visualFeatures = new VisualFeature[] { VisualFeature.Adult, VisualFeature.Categories, VisualFeature.Color, VisualFeature.Description, VisualFeature.Faces, VisualFeature.ImageType, VisualFeature.Tags };
            vrbugwinapp.Contract.AnalysisResult analysisResult = await VisionServiceClient.AnalyzeImageAsync(imageUrl, visualFeatures);
            return analysisResult;

           
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VisionServiceClient"/> class.
        /// </summary>
        /// <param name="APIKey">The subscription key.</param>


        public MSOxfordClient(string apiKey)
        {
            APIKey = apiKey;
        }


        /// <summary>
        /// This operation extracts a rich set of visual features based on the image content. 
        /// Input requirements:
        /// Supported image formats: JPEG, PNG, GIF, BMP.
        /// Image file size must be less than 4MB.
        /// Image dimensions must be at least 50 x 50.
        /// visualFeatures (optional) string A string indicating what visual feature types to return. 
        /// Multiple values should be comma-separated. 
        /// Valid visual feature types include: 
        /// Categories - categorizes image content according to a taxonomy defined in documentation.
        /// Tags - tags the image with a detailed list of words related to the image content.
        /// Description - describes the image content with a complete English sentence.
        /// Faces - detects if faces are present.If present, generate coordinates, gender and age.ImageType - detects if image is clipart or a line drawing.
        /// Color - determines the accent color, dominant color, and whether an image is black&white.
        /// Adult - detects if the image is pornographic in nature (depicts nudity or a sex act). Sexually suggestive content is also detected.        
        /// </summary>
        public object AnalyzeImageCurl(string imageUrl, string visualFeatures)
        {
            
            AnalysisResult analyzeImageResponse = null;
            string result = "";
            try
            {
                //curl -v -X POST "https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures=Categories&details={string}&language=en"
                //-H "Content-Type: application/json"
                //-H "Ocp-Apim-Subscription-Key: {subscription key}"

                //--data-ascii "{body}"

                string visualFeaturesList = string.Join(",", visualFeatures);
                string body = "{'url':'" + imageUrl + "'}";

                Stopwatch stopwatch = new Stopwatch();
                string tagArguments = string.Format(" -v -X POST \"https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures={0}&language=en\" -H \"Content-Type: application/json\" -H \"Ocp-Apim-Subscription-Key: {1}\" --data-ascii \"{2}\"", visualFeaturesList, APIKey, body);

                stopwatch.Start();
                result = Util.RunProcess("curl.exe", tagArguments);
                stopwatch.Stop();
                //Console.WriteLine(String.Format("Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds));

                analyzeImageResponse = JsonConvert.DeserializeObject<AnalysisResult>(result);
            }
            catch (Exception ex)
            {
                throw;
            }
            return analyzeImageResponse;
    }

        /// <summary>
        /// This operation extracts a rich set of visual features based on the image content. 
        /// Input requirements:
        /// Supported image formats: JPEG, PNG, GIF, BMP.
        /// Image file size must be less than 4MB.
        /// Image dimensions must be at least 50 x 50.
        /// visualFeatures (optional) string A string indicating what visual feature types to return. 
        /// Multiple values should be comma-separated. 
        /// Valid visual feature types include: 
        /// Categories - categorizes image content according to a taxonomy defined in documentation.
        /// Tags - tags the image with a detailed list of words related to the image content.
        /// Description - describes the image content with a complete English sentence.
        /// Faces - detects if faces are present.If present, generate coordinates, gender and age.ImageType - detects if image is clipart or a line drawing.
        /// Color - determines the accent color, dominant color, and whether an image is black&white.
        /// Adult - detects if the image is pornographic in nature (depicts nudity or a sex act). Sexually suggestive content is also detected.        
        /// </summary>
        public string AnalyzeImageByUrl(string imageUrl, List<string> visualFeatures = null)
        {
            
            //var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            //HttpResponseMessage response;
            string result = "";
            try
            {
                if (visualFeatures == null)
                    visualFeatures = new List<string>() { "Categories" };

                // Request headers
                //client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", APIKey);


                string visualFeaturesList = string.Join(",", visualFeatures);

                var client = new RestClient("https://api.projectoxford.ai/vision/v1.0/analyze");
                // client.Authenticator = new HttpBasicAuthenticator(username, password);

                var request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.Parameters.Clear();
                request.AddParameter("visualFeatures", visualFeaturesList); // adds to POST or URL querystring based on Method
                request.AddParameter("language", "en");
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("Ocp-Apim-Subscription-Key", APIKey);

                //MSOxfordBody bodyJson = new MSOxfordBody(imageUrl);
                //request.AddJsonBody(bodyJson);
                //string body = "{\"url\":\"" + imageUrl + "\"}";

                request.RequestFormat = DataFormat.Json;
                request.AddBody(new { url = imageUrl }); // uses JsonSerializer               
                //request.AddParameter("application/json", body, ParameterType.RequestBody);

                string fullRest = Util.GetFullRestUrl(client, request);
                Logger.log.Debug("MsOxford full Rest url for image analysis \n" + fullRest);
                // execute the request
                IRestResponse response = client.Execute(request);
                result = response.Content; // raw content as string





                //// Request parameters
                //queryString["visualFeatures"] = visualFeaturesList;
                //queryString["details"] = "{string}";
                //queryString["language"] = "en";
                //var uri = "https://api.projectoxford.ai/vision/v1.0/analyze?" + queryString;


                // Request body
                //byte[] byteData = Encoding.UTF8.GetBytes("{\"url\":\""+ imageUrl +"\"}");

                //using (var content = new ByteArrayContent(byteData))
                //{
                //    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                //    response = await client.PostAsync(uri, content);
                //}
            }
            catch (Exception)
            {
                throw;
            }

            return result;
        }

        /// <summary>
        /// This operation generates a description of an image in human readable language with complete sentences. 
        /// The description is based on a collection of content tags, which are also returned by the operation. 
        /// More than one description can be generated for each image. Descriptions are ordered by their confidence score. 
        /// All descriptions are in English. 
        /// </summary>
        /// <returns></returns>
        public async Task<string> DescribeImage()
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            HttpResponseMessage response;

            try
            {
                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", APIKey);

                // Request parameters
                queryString["maxCandidates"] = "1";
                var uri = "https://api.projectoxford.ai/vision/v1.0/describe?" + queryString;


                // Request body
                byte[] byteData = Encoding.UTF8.GetBytes("{body}");

                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    response = await client.PostAsync(uri, content);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return response.Content.ToString();
        }

        /// <summary>
        /// The service host
        /// </summary>
        private const string ServiceHost = "https://api.projectoxford.ai/vision/v1";

        /// <summary>
        /// The analyze query
        /// </summary>
        private const string AnalyzeQuery = "analyze";

        /// <summary>
        /// The subscription key name
        /// </summary>
        private const string APIKeyName = "subscription-key";

        /// <summary>
        /// The default resolver
        /// </summary>
        private CamelCasePropertyNamesContractResolver _defaultResolver = new CamelCasePropertyNamesContractResolver();


        /// <summary>
        /// Analyzes the image.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="visualFeatures">The visual features.</param>
        /// <returns>The AnalysisResult object.</returns>
        public async Task<AnalysisResult> AnalyzeImageAsync(string url, string[] visualFeatures = null)
        {
            string requestUrl = string.Format("{0}/{1}?visualFeatures={2}&{3}={4}", ServiceHost, AnalyzeQuery, VisualFeaturesToString(visualFeatures), APIKeyName, APIKey);
            var request = WebRequest.Create(requestUrl);

            dynamic requestObject = new ExpandoObject();
            requestObject.url = url;

            return await this.SendAsync<ExpandoObject, AnalysisResult>("POST", requestObject, request);
        }

        /// <summary>
        /// Analyzes the image.
        /// </summary>
        /// <param name="imageStream">The image stream.</param>
        /// <param name="visualFeatures">The visual features.</param>
        /// <returns>The AnalysisResult object.</returns>
        public async Task<AnalysisResult> AnalyzeImageAsync(Stream imageStream, string[] visualFeatures = null)
        {
            string requestUrl = string.Format("{0}/{1}?visualFeatures={2}&{3}={4}", ServiceHost, AnalyzeQuery, VisualFeaturesToString(visualFeatures), APIKeyName, APIKey);
            var request = WebRequest.Create(requestUrl);

            return await this.SendAsync<Stream, AnalysisResult>("POST", imageStream, request);
        }

        /// <summary>
        /// Gets the thumbnail.
        /// </summary>
        /// <param name="url">The URL.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="smartCropping">if set to <c>true</c> [smart cropping].</param>
        /// <returns>Image bytes</returns>
        public async Task<byte[]> GetThumbnailAsync(string url, int width, int height, bool smartCropping = true)
        {
            string requestUrl = string.Format("{0}/thumbnails?width={1}&height={2}&smartCropping={3}&{4}={5}", ServiceHost, width, height, smartCropping, APIKeyName, APIKey);
            var request = WebRequest.Create(requestUrl);

            dynamic requestObject = new ExpandoObject();
            requestObject.url = url;

            return await this.SendAsync<ExpandoObject, byte[]>("POST", requestObject, request);
        }

        /// <summary>
        /// Gets the thumbnail.
        /// </summary>
        /// <param name="stream">The stream.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <param name="smartCropping">if set to <c>true</c> [smart cropping].</param>
        /// <returns>Image bytes</returns>
        public async Task<byte[]> GetThumbnailAsync(Stream stream, int width, int height, bool smartCropping = true)
        {
            string requestUrl = string.Format("{0}/thumbnails?width={1}&height={2}&smartCropping={3}&{4}={5}", ServiceHost, width, height, smartCropping, APIKeyName, APIKey);
            var request = WebRequest.Create(requestUrl);

            return await this.SendAsync<Stream, byte[]>("POST", stream, request);
        }

        /// <summary>
        /// Recognizes the text asynchronous.
        /// </summary>
        /// <param name="imageUrl">The image URL.</param>
        /// <param name="languageCode">The language code.</param>
        /// <param name="detectOrientation">if set to <c>true</c> [detect orientation].</param>
        /// <returns>The OCR object.</returns>
        public async Task<OcrResults> RecognizeTextAsync(string imageUrl, string languageCode = LanguageCodes.AutoDetect, bool detectOrientation = true)
        {
            string requestUrl = string.Format("{0}/ocr?language={1}&detectOrientation={2}&{3}={4}", ServiceHost, languageCode, detectOrientation, APIKeyName, APIKey);
            var request = WebRequest.Create(requestUrl);

            dynamic requestObject = new ExpandoObject();
            requestObject.url = imageUrl;

            return await this.SendAsync<ExpandoObject, OcrResults>("POST", requestObject, request);
        }

        /// <summary>
        /// Recognizes the text asynchronous.
        /// </summary>
        /// <param name="imageStream">The image stream.</param>
        /// <param name="languageCode">The language code.</param>
        /// <param name="detectOrientation">if set to <c>true</c> [detect orientation].</param>
        /// <returns>The OCR object.</returns>
        public async Task<OcrResults> RecognizeTextAsync(Stream imageStream, string languageCode = LanguageCodes.AutoDetect, bool detectOrientation = true)
        {
            string requestUrl = string.Format("{0}/ocr?language={1}&detectOrientation={2}&{3}={4}", ServiceHost, languageCode, detectOrientation, APIKeyName, APIKey);
            var request = WebRequest.Create(requestUrl);

            return await this.SendAsync<Stream, OcrResults>("POST", imageStream, request);
        }

        /// <summary>
        /// Strings the array to string.
        /// </summary>
        /// <param name="string">The @string.</param>
        /// <returns>The visual features string.</returns>
        private string VisualFeaturesToString(string[] @string)
        {
            if (null == @string)
            {
                return "All";
            }

            var sb = new StringBuilder();

            foreach (var s in @string)
            {
                sb.Append(s).Append(',');
            }

            return sb.ToString().TrimEnd(',');
        }

        #region the json client

        /// <summary>
        /// Gets the asynchronous.
        /// </summary>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="method">The method.</param>
        /// <param name="request">The request.</param>
        /// <param name="setHeadersCallback">The set headers callback.</param>
        /// <returns>
        /// The response object.
        /// </returns>
        private async Task<TResponse> GetAsync<TResponse>(string method, WebRequest request, Action<WebRequest> setHeadersCallback = null)
        {
            if (request == null)
            {
                new ArgumentNullException("request");
            }

            try
            {
                request.Method = method;
                if (null == setHeadersCallback)
                {
                    this.SetCommonHeaders(request);
                }
                else
                {
                    setHeadersCallback(request);
                }

                var response = await Task.Factory.FromAsync<WebResponse>(
                    request.BeginGetResponse,
                    request.EndGetResponse,
                    null);

                return this.ProcessAsyncResponse<TResponse>(response as HttpWebResponse);
            }
            catch (Exception e)
            {
                this.HandleException(e);
                return default(TResponse);
            }
        }

        /// <summary>
        /// Sends the asynchronous.
        /// </summary>
        /// <typeparam name="TRequest">The type of the request.</typeparam>
        /// <typeparam name="TResponse">The type of the response.</typeparam>
        /// <param name="method">The method.</param>
        /// <param name="requestBody">The request body.</param>
        /// <param name="request">The request.</param>
        /// <param name="setHeadersCallback">The set headers callback.</param>
        /// <returns>The response object.</returns>
        /// <exception cref="System.ArgumentNullException">request</exception>
        private async Task<TResponse> SendAsync<TRequest, TResponse>(string method, TRequest requestBody, WebRequest request, Action<WebRequest> setHeadersCallback = null)
        {
            try
            {
                if (request == null)
                {
                    throw new ArgumentNullException("request");
                }

                request.Method = method;
                if (null == setHeadersCallback)
                {
                    this.SetCommonHeaders(request);
                }
                else
                {
                    setHeadersCallback(request);
                }

                if (requestBody is Stream)
                {
                    request.ContentType = "application/octet-stream";
                }

                var asyncState = new WebRequestAsyncState()
                {
                    RequestBytes = this.SerializeRequestBody(requestBody),
                    WebRequest = (HttpWebRequest)request,
                };

                var continueRequestAsyncState = await Task.Factory.FromAsync<Stream>(
                                                    asyncState.WebRequest.BeginGetRequestStream,
                                                    asyncState.WebRequest.EndGetRequestStream,
                                                    asyncState,
                                                    TaskCreationOptions.None).ContinueWith<WebRequestAsyncState>(
                                                       task =>
                                                       {
                                                           var requestAsyncState = (WebRequestAsyncState)task.AsyncState;
                                                           if (requestBody != null)
                                                           {
                                                               using (var requestStream = task.Result)
                                                               {
                                                                   if (requestBody is Stream)
                                                                   {
                                                                       (requestBody as Stream).CopyTo(requestStream);
                                                                   }
                                                                   else
                                                                   {
                                                                       requestStream.Write(requestAsyncState.RequestBytes, 0, requestAsyncState.RequestBytes.Length);
                                                                   }
                                                               }
                                                           }

                                                           return requestAsyncState;
                                                       });

                var continueWebRequest = continueRequestAsyncState.WebRequest;
                var response = await Task.Factory.FromAsync<WebResponse>(
                                            continueWebRequest.BeginGetResponse,
                                            continueWebRequest.EndGetResponse,
                                            continueRequestAsyncState);

                return this.ProcessAsyncResponse<TResponse>(response as HttpWebResponse);
            }
            catch (Exception e)
            {
                this.HandleException(e);
                return default(TResponse);
            }
        }

        /// <summary>
        /// Processes the asynchronous response.
        /// </summary>
        /// <typeparam name="T">Type of response.</typeparam>
        /// <param name="webResponse">The web response.</param>
        /// <returns>The response.</returns>
        private T ProcessAsyncResponse<T>(HttpWebResponse webResponse)
        {
            using (webResponse)
            {
                if (webResponse.StatusCode == HttpStatusCode.OK ||
                    webResponse.StatusCode == HttpStatusCode.Accepted ||
                    webResponse.StatusCode == HttpStatusCode.Created)
                {
                    if (webResponse.ContentLength != 0)
                    {
                        using (var stream = webResponse.GetResponseStream())
                        {
                            if (stream != null)
                            {
                                if (webResponse.ContentType == "image/jpeg" ||
                                    webResponse.ContentType == "image/png")
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        stream.CopyTo(ms);
                                        return (T)(object)ms.ToArray();
                                    }
                                }
                                else
                                {
                                    string message = string.Empty;
                                    using (StreamReader reader = new StreamReader(stream))
                                    {
                                        message = reader.ReadToEnd();
                                    }

                                    JsonSerializerSettings settings = new JsonSerializerSettings();
                                    settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
                                    settings.NullValueHandling = NullValueHandling.Ignore;
                                    settings.ContractResolver = this._defaultResolver;

                                    return JsonConvert.DeserializeObject<T>(message, settings);
                                }
                            }
                        }
                    }
                }
            }

            return default(T);
        }

        /// <summary>
        /// Set request content type.
        /// </summary>
        /// <param name="request">Web request object.</param>
        private void SetCommonHeaders(WebRequest request)
        {
            request.ContentType = "application/json";
            request.Headers.Add("Ocp-Apim-Subscription-Key", APIKey);

        }

        /// <summary>
        /// Serialize the request body to byte array.
        /// </summary>
        /// <typeparam name="T">Type of request object.</typeparam>
        /// <param name="requestBody">Strong typed request object.</param>
        /// <returns>Byte array.</returns>
        private byte[] SerializeRequestBody<T>(T requestBody)
        {
            if (requestBody == null || requestBody is Stream)
            {
                return null;
            }
            else
            {
                JsonSerializerSettings settings = new JsonSerializerSettings();
                settings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
                settings.ContractResolver = this._defaultResolver;

                return System.Text.Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(requestBody, settings));
            }
        }

        /// <summary>
        /// Process the exception happened on rest call.
        /// </summary>
        /// <param name="exception">Exception object.</param>
        private void HandleException(Exception exception)
        {
            WebException webException = exception as WebException;
            if (webException != null && webException.Response != null)
            {
                if (webException.Response.ContentType.ToLower().Contains("application/json"))
                {
                    Stream stream = null;

                    try
                    {
                        stream = webException.Response.GetResponseStream();
                        if (stream != null)
                        {
                            string errorObjectString;
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                stream = null;
                                errorObjectString = reader.ReadToEnd();
                            }

                            ClientError errorCollection = JsonConvert.DeserializeObject<ClientError>(errorObjectString);
                            if (errorCollection != null)
                            {
                                throw new ClientException
                                {
                                    Error = errorCollection,
                                };
                            }
                        }
                    }
                    finally
                    {
                        if (stream != null)
                        {
                            stream.Dispose();
                        }
                    }
                }
            }

            throw exception;
        }

        /// <summary>
        /// This class is used to pass on "state" between each Begin/End call
        /// It also carries the user supplied "state" object all the way till
        /// the end where is then hands off the state object to the
        /// WebRequestCallbackState object.
        /// </summary>
        internal class WebRequestAsyncState
        {
            /// <summary>
            /// Gets or sets request bytes of the request parameter for http post.
            /// </summary>
            public byte[] RequestBytes { get; set; }

            /// <summary>
            /// Gets or sets the HttpWebRequest object.
            /// </summary>
            public HttpWebRequest WebRequest { get; set; }

            /// <summary>
            /// Gets or sets the request state object.
            /// </summary>
            public object State { get; set; }
        }

        #endregion
    }


    /// <summary>
    /// Container of ClientError and Error Entity.
    /// </summary>
    public class ClientError
    {
        /// <summary>
        /// Gets or sets error code in error entity.
        /// </summary>
        public string Code
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>
        /// The message.
        /// </value>
        public string Message
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the request identifier.
        /// </summary>
        /// <value>
        /// The request identifier.
        /// </value>
        public Guid RequestId { get; set; }
    }

    /// <summary>
    /// The Exception will be shown to client.
    /// </summary>
    public class ClientException : Exception
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ClientException"/> class.
        /// </summary>
        public ClientException()
            : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientException"/> class.
        /// </summary>
        /// <param name="message">The corresponding error message.</param>
        public ClientException(string message)
            : base(message)
        {
            this.Error = new ClientError()
            {
                Code = HttpStatusCode.InternalServerError.ToString(),
                Message = message
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientException"/> class.
        /// </summary>
        /// <param name="message">The corresponding error message.</param>
        /// <param name="httpStatus">The Http Status code.</param>
        public ClientException(string message, HttpStatusCode httpStatus)
            : base(message)
        {
            this.HttpStatus = httpStatus;

            this.Error = new ClientError()
            {
                Code = this.HttpStatus.ToString(),
                Message = message
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientException"/> class.
        /// </summary>
        /// <param name="message">The corresponding error message.</param>
        /// <param name="innerException">The inner exception.</param>
        public ClientException(string message, Exception innerException)
            : base(message, innerException)
        {
            this.Error = new ClientError()
            {
                Code = HttpStatusCode.InternalServerError.ToString(),
                Message = message
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientException"/> class.
        /// </summary>
        /// <param name="message">The corresponding error message.</param>
        /// <param name="errorCode">The error code.</param>
        /// <param name="httpStatus">The http status.</param>
        /// <param name="innerException">The inner exception.</param>
        public ClientException(string message, string errorCode, HttpStatusCode httpStatus, Exception innerException)
            : base(message, innerException)
        {
            this.HttpStatus = httpStatus;

            this.Error = new ClientError()
            {
                Code = errorCode,
                Message = message
            };
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ClientException"/> class.
        /// </summary>
        /// <param name="error">The error entity.</param>
        /// <param name="httpStatus">The http status.</param>
        public ClientException(ClientError error, HttpStatusCode httpStatus)
        {
            this.Error = error;
            this.HttpStatus = httpStatus;
        }

        /// <summary>
        /// Gets http status of http response.
        /// </summary>
        public HttpStatusCode HttpStatus
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets or sets the httpError message.
        /// </summary>
        public ClientError Error { get; set; }

        /// <summary>
        /// Create Client Exception of Bad Request.
        /// </summary>
        /// <param name="message">The corresponding error message.</param>
        /// <returns>Client Exception Instance.</returns>
        public static ClientException BadRequest(string message)
        {
            return new ClientException(
                         new ClientError()
                         {
                             Code = ((int)HttpStatusCode.BadRequest).ToString(),
                             Message = message
                         },
                         HttpStatusCode.BadRequest);
        }
    }
}
