﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Web;
using System.Threading.Tasks;

namespace MSOxford
{
    public class MSOxfordClient
    {
        string APIKey { get; set; }
        public enum VisualFeatures { ImageType, Faces, Adult, Categories, Color, Tags, Description };

        public MSOxfordClient(string apiKey)
        {
            APIKey = apiKey;
        }


        /// <summary>
        /// This operation extracts a rich set of visual features based on the image content. 
        /// Input requirements:
        /// Supported image formats: JPEG, PNG, GIF, BMP.
        /// Image file size must be less than 4MB.
        /// Image dimensions must be at least 50 x 50.
        /// visualFeatures (optional) string A string indicating what visual feature types to return. 
        /// Multiple values should be comma-separated. 
        /// Valid visual feature types include: 
        /// Categories - categorizes image content according to a taxonomy defined in documentation.
        /// Tags - tags the image with a detailed list of words related to the image content.
        /// Description - describes the image content with a complete English sentence.
        /// Faces - detects if faces are present.If present, generate coordinates, gender and age.ImageType - detects if image is clipart or a line drawing.
        /// Color - determines the accent color, dominant color, and whether an image is black&white.
        /// Adult - detects if the image is pornographic in nature (depicts nudity or a sex act). Sexually suggestive content is also detected.        
        /// </summary>
        public void AnalyzeImageCurl(List<string> visualFeatures = null)
        {
            if (visualFeatures == null)
                visualFeatures = new List<string>() { "Categories" };
            AnalyzeImageResponse analyzeImageResponse = null;

            try
            {
                //curl -v -X POST "https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures=Categories&details={string}&language=en"
                //-H "Content-Type: application/json"
                //-H "Ocp-Apim-Subscription-Key: {subscription key}"

                //--data-ascii "{body}"

                string visualFeaturesList = string.Join(",", visualFeatures);
               
                Stopwatch stopwatch = new Stopwatch();
                string tagArguments = string.Format(" -v -X POST \"https://api.projectoxford.ai/vision/v1.0/analyze?visualFeatures={0}&details={string}&language=en\" -H \"Content-Type: application/json\" -H \"Ocp-Apim-Subscription-Key: {1}\"", visualFeaturesList, APIKey);

                stopwatch.Start();
                string analyzeResults = Util.RunProcess("curl.exe", tagArguments);
                stopwatch.Stop();
                Console.WriteLine(String.Format("Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds));

                analyzeImageResponse = JsonConvert.DeserializeObject<AnalyzeImageResponse>(analyzeResults);
            }
            catch (Exception)
            {

                throw;
            }
    }

        /// <summary>
        /// This operation extracts a rich set of visual features based on the image content. 
        /// Input requirements:
        /// Supported image formats: JPEG, PNG, GIF, BMP.
        /// Image file size must be less than 4MB.
        /// Image dimensions must be at least 50 x 50.
        /// visualFeatures (optional) string A string indicating what visual feature types to return. 
        /// Multiple values should be comma-separated. 
        /// Valid visual feature types include: 
        /// Categories - categorizes image content according to a taxonomy defined in documentation.
        /// Tags - tags the image with a detailed list of words related to the image content.
        /// Description - describes the image content with a complete English sentence.
        /// Faces - detects if faces are present.If present, generate coordinates, gender and age.ImageType - detects if image is clipart or a line drawing.
        /// Color - determines the accent color, dominant color, and whether an image is black&white.
        /// Adult - detects if the image is pornographic in nature (depicts nudity or a sex act). Sexually suggestive content is also detected.        
        /// </summary>
        public async Task<string> AnalyzeImageByUrl(string imageUrl, List<string> visualFeatures = null)
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            HttpResponseMessage response;

            try
            {
                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", APIKey);
                string visualFeaturesList = string.Join(",", visualFeatures);

                // Request parameters
                queryString["visualFeatures"] = visualFeaturesList;
                queryString["details"] = "{string}";
                queryString["language"] = "en";
                var uri = "https://api.projectoxford.ai/vision/v1.0/analyze?" + queryString;


                // Request body
                byte[] byteData = Encoding.UTF8.GetBytes("{\"url\":\""+ imageUrl +"\"}");

                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    response = await client.PostAsync(uri, content);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return response.Content.ToString();
        }

        /// <summary>
        /// This operation generates a description of an image in human readable language with complete sentences. 
        /// The description is based on a collection of content tags, which are also returned by the operation. 
        /// More than one description can be generated for each image. Descriptions are ordered by their confidence score. 
        /// All descriptions are in English. 
        /// </summary>
        /// <returns></returns>
        public async Task<string> DescribeImage()
        {
            var client = new HttpClient();
            var queryString = HttpUtility.ParseQueryString(string.Empty);
            HttpResponseMessage response;

            try
            {
                // Request headers
                client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", APIKey);

                // Request parameters
                queryString["maxCandidates"] = "1";
                var uri = "https://api.projectoxford.ai/vision/v1.0/describe?" + queryString;


                // Request body
                byte[] byteData = Encoding.UTF8.GetBytes("{body}");

                using (var content = new ByteArrayContent(byteData))
                {
                    content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    response = await client.PostAsync(uri, content);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return response.Content.ToString();
        }
    }
}
