﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Clarifi
{
    class Util
    {
        public static string RunProcess(string processFileName, string arguments)
        {
            StringBuilder result = new StringBuilder();

            try
            {
                var proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = processFileName,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    }
                };
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                proc.Start();
                stopwatch.Stop();

                while (!proc.StandardOutput.EndOfStream)
                {
                    string line = proc.StandardOutput.ReadLine();
                    result.AppendLine(line);
                    Console.WriteLine(line);
                }
                Console.WriteLine(string.Format("Elapsed Time (ms): {0}", stopwatch.ElapsedMilliseconds));
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result.ToString();
        }
    }
}
