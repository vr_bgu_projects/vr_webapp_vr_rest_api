﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Clarifi
{

    public class ClarifiTagResponse
    {
        public string status_code { get; set; }
        public string status_msg { get; set; }
        public Meta meta { get; set; }
        public List<Result> results { get; set; }
    }

    public class Tag
    {
        public double timestamp { get; set; }
        public string model { get; set; }
        public object config { get; set; }
    }

    public class Meta
    {
        public Tag tag { get; set; }
    }

    public class Result
    {
        public double docid;
        public string url;
        public string status_code;
        public string status_msg;
        public string local_id;
        public Result_ result;
        public string docidStr;
        public string mimeType;
    }

    public class Result_
    {
        public Tag_ tag;
    }

    public class Tag_
    {

        public List<double> timestamps = new List<double>();
        public List<List<string>> classes = new List<List<string>>();
        public List<List<string>> conceptIds = new List<List<string>>();
        public List<List<double>> probs = new List<List<double>>();
    }
}