﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Clarifi
{
    public class ClarifiClient
    {

        string Clarifi_API_Root{ get; set; }
        string Client_Id { get; set; }
        string Client_Secret { get; set; }

       
        public ClarifiClient(string clarifi_api_root, string client_id, string client_secret)
        {
            Clarifi_API_Root = clarifi_api_root;
            Client_Id = client_id;
            Client_Secret = client_secret;       
        }


        public ClarifiTagResponse TagImageUrl(SourceImage sourceImage)
        {
            ClarifiTagResponse clarifiTagResponse = null;
            try
            {
                ClarifaiAccessToken clarifaiAccessToken = GetClarifaiAccesstoken();
                // Create new stopwatch.
                Stopwatch stopwatch = new Stopwatch();

                // Begin timing.
                stopwatch.Start();
                string tagArguments = string.Format("\"{0}tag/?url={1}\" -H \"Authorization: Bearer {2}\"", this.Clarifi_API_Root, sourceImage.ImageUrl, clarifaiAccessToken.access_token);
                string tagResults = Util.RunProcess("curl.exe", tagArguments);
                stopwatch.Stop();
                Console.WriteLine(String.Format("Time Elapsed: {0} ms", stopwatch.Elapsed.TotalMilliseconds) );

                clarifiTagResponse = JsonConvert.DeserializeObject<ClarifiTagResponse>(tagResults);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clarifiTagResponse;

        }


        public ClarifaiAccessToken GetClarifaiAccesstoken()
        {
            ClarifaiAccessToken clarifaiAccessToken = null;
            try
            {
                string processFileName = "curl.exe";
                string arguments = string.Format("-X POST \"{0}token/\" -d \"client_id={1}\" -d \"client_secret={2}\" -d \"grant_type=client_credentials\"", Clarifi_API_Root, Client_Id, Client_Secret);
                string result = Util.RunProcess(processFileName, arguments);
                clarifaiAccessToken = JsonConvert.DeserializeObject<ClarifaiAccessToken>(result);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return clarifaiAccessToken;
        }


    }
}
